/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy;

import fr.creerio.duy.commands.subcommands.settings.location.Show;
import fr.creerio.duy.events.inventories.Items;
import fr.creerio.duy.events.inventories.ItemsRead;
import fr.creerio.duy.events.listeners.DamagedArmorStandRemove;
import fr.creerio.duy.events.listeners.InGame;
import fr.creerio.duy.events.listeners.InGamePlayer;
import fr.creerio.duy.events.listeners.RefreshShowOnAction;
import fr.creerio.duy.modules.CommandHandler;
import fr.creerio.duy.modules.Config;
import fr.creerio.duy.modules.Game;
import fr.creerio.duy.modules.SpawnLocation;
import fr.creerio.duy.utils.LocationSerializable;
import fr.creerio.duy.utils.MaterialWHeight;
import fr.creerio.duy.utils.SoundData;
import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.List;

public final class DUY extends JavaPlugin {

    public static final String TAG = "[DUY]";

    private static NamespacedKey key;

    /**
     * Used to add commands
     */
    private final CommandHandler cmdHandler = new CommandHandler();

    public static NamespacedKey getPluginKey() {
        return key;
    }

    @Override
    public void onEnable() {
        key = new NamespacedKey(this, "duy");

        // Create configuration object
        ConfigurationSerialization.registerClass(LocationSerializable.class);
        ConfigurationSerialization.registerClass(MaterialWHeight.class);
        ConfigurationSerialization.registerClass(SoundData.class);
        new Config(this);

        // Events
        List<Listener> events = Arrays.asList(new RefreshShowOnAction(), new DamagedArmorStandRemove(), new InGamePlayer(), new InGame(), new ItemsRead(), new Items());
        for (Listener event : events) {
            Bukkit.getPluginManager().registerEvents(event, this);
        }

        // Commands
        cmdHandler.addCommandsToPlugin(this);

        // Create SpawnLocation instance, verifies the currently stored locations & current height
        SpawnLocation.getInstance();

        // Reset blocks
        SpawnLocation.getInstance().get().forEach(Game.instance::generateTopBlocks);
    }

    @Override
    public void onDisable() {
        Show.removeArmorStands();
        Config.save();
    }
}
