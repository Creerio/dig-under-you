/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.events.listeners;

import fr.creerio.duy.DUY;
import fr.creerio.duy.commands.subcommands.settings.location.Show;
import fr.creerio.duy.modules.*;
import org.bukkit.ChatColor;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class DamagedArmorStandRemove implements Listener {
    @EventHandler
    public void removeArmorStandOnHit(final EntityDamageByEntityEvent event) {
        if (event.isCancelled() || !event.getDamager().hasPermission("duy.settings") || !event.getDamager().hasPermission("duy.settings.spawn"))
            return;

        if (event.getDamager() instanceof Player && event.getEntity() instanceof ArmorStand && event.getEntity().getScoreboardTags().contains(DUY.TAG)) {
            if (!Game.instance.isEmpty()) {
                event.getDamager().sendMessage(ChatColor.RED + Language.getTranslation(Language.TranslationKey.IN_GAME_PLAYER));
                event.setCancelled(true);
                return;
            }

            SpawnLocation.getInstance().removeLocation(event.getEntity().getLocation());
            Show.refresh();

            if (SaveType.getType(Config.getFile().getString(Config.ConfigKey.SAVE_TYPE.toString())) == SaveType.ON_UPDATE)
                Config.save();
        }
    }
}
