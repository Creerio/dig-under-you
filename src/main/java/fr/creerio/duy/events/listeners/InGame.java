/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.events.listeners;

import fr.creerio.duy.events.InGameBlockBroken;
import fr.creerio.duy.events.InGameBlockGeneration;
import fr.creerio.duy.modules.Config;
import fr.creerio.duy.modules.Game;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class InGame implements Listener {

    @EventHandler
    public void blockBroken(final InGameBlockBroken event) {
        if (event.isCancelled())
            return;

        Game.instance.addMinedBlock(event.getPlayer(), event.getBlock().getType());

        if (Config.getFile().getBoolean(Config.ConfigKey.ENABLE_GAME_MESSAGES.toString()))
            event.getPlayer().spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(ChatColor.GOLD + "+ 1x " + event.getBlock().getType()));
        Game.instance.updateScoreboard(event.getPlayer());

        Bukkit.getPluginManager().callEvent(new InGameBlockGeneration(event.getPlayer(), event.getBlock().getLocation()));
    }

    @EventHandler
    public void blockGeneration(final InGameBlockGeneration event) {
        if (event.isCancelled())
            return;

        Game.instance.generateBottomBlock(event.getPlayer(), event.getLocation());
    }
}
