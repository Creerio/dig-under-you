/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.events.listeners;

import fr.creerio.duy.events.InGameBlockBroken;
import fr.creerio.duy.modules.Config;
import fr.creerio.duy.modules.Config.ConfigKey;
import fr.creerio.duy.events.InGameJoin;
import fr.creerio.duy.events.InGameLavaSpawn;
import fr.creerio.duy.events.InGameLeave;
import fr.creerio.duy.modules.Game;
import fr.creerio.duy.modules.GameUtils;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.utils.LocationSerializable;
import fr.creerio.duy.utils.SoundData;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.*;

public class InGamePlayer implements Listener {

    /**
     * Used to do different actions on the player should the joining event not be cancelled
     *
     * @param event
     * Join event, launched with the /game join command
     */
    @EventHandler
    public void playerJoined(final InGameJoin event) {
        if (event.isCancelled()) {
            Game.instance.removePlayer(event.getPlayer(), true);
            return;
        }

        Player player = event.getPlayer();

        // Food & health
        player.setFoodLevel(20);
        player.setSaturation(20);
        player.setHealth(player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getDefaultValue());

        // TP, gamemode, inventory & scoreboard
        player.teleport(event.getLocation());
        player.setGameMode(GameMode.SURVIVAL);
        event.getPlayer().setCanPickupItems(false);
        player.getInventory().clear();
        player.getInventory().setItem(8, GameUtils.getInstance().getQuitItem());
        player.getInventory().setHeldItemSlot(0);
        GameUtils.getInstance().getStartingItems().forEach(itemStack -> player.getInventory().addItem(itemStack));
        Game.instance.addPlayerScoreboard(event.getPlayer());

        if (Config.getFile().getBoolean(ConfigKey.ENABLE_GAME_MESSAGES.toString()))
            Game.instance.broadcastActionBarMessage(TextComponent.fromLegacyText(ChatColor.GOLD + Language.getTranslation(Language.TranslationKey.IN_GAME_JOIN, player.getDisplayName())));

        if (Config.getFile().getBoolean(ConfigKey.SOUND_ENABLED.toString()))
            Game.instance.broadcastSound((SoundData) Config.getFile().get(ConfigKey.SOUND_PLAYER_JOIN.toString()));
    }

    @EventHandler
    public void playerLeft(final InGameLeave event) {
        if (event.isCancelled())
            return;

        Player player = event.getPlayer();

        player.getInventory().clear();
        player.setCanPickupItems(true);
        Game.instance.removePlayerScoreboard(event.getPlayer());

        if (Config.getFile().getBoolean(ConfigKey.RESPAWN_ENABLED.toString()))
            player.teleport(((LocationSerializable) Config.getFile().get(ConfigKey.RESPAWN_LOCATION.toString())).toLocation());


        // Food & health
        player.setFoodLevel(20);
        player.setSaturation(20);
        player.setHealth(player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getDefaultValue());
        player.setFireTicks(0);

        if (event.getSurvived() && Config.getFile().getBoolean(ConfigKey.ENABLE_GAME_MESSAGES.toString())) {
            if (event.getMinedBlocks().isEmpty())
                player.sendMessage(Language.getTranslation(Language.TranslationKey.IN_GAME_SURVIVED_NOTHING));
            else {
                player.sendMessage(Language.getTranslation(Language.TranslationKey.IN_GAME_SURVIVED_BLOCKS));
                event.getMinedBlocks().forEach((material, quantity) -> player.sendMessage(Language.getTranslation(Language.TranslationKey.LIST_SYMBOL, (quantity + "x " + material))));
            }
        }
    }

    @EventHandler
    public void removePlayerOnQuit(final PlayerQuitEvent event) {
        if (Game.instance.isPlayerInGame(event.getPlayer())) {
            Game.instance.removePlayer(event.getPlayer(), true);
            event.getPlayer().getInventory().clear();

            if (Config.getFile().getBoolean(ConfigKey.RESPAWN_ENABLED.toString()))
                event.getPlayer().teleport(((LocationSerializable) Config.getFile().get(ConfigKey.RESPAWN_LOCATION.toString())).toLocation());
        }
    }

    /************************************************************

                            Player actions

     ************************************************************/

    @EventHandler
    public void playerMoved(final PlayerMoveEvent event) {
        if (event.isCancelled())
            return;

        if (Game.instance.isPlayerInGame(event.getPlayer()))
            Game.instance.checkLocation(event.getPlayer(), event.getTo() != null ? event.getTo() : event.getPlayer().getLocation());
    }

    @EventHandler
    public void playerBreakBlock(final BlockBreakEvent event) {
        if (event.isCancelled() || !Game.instance.isPlayerInGame(event.getPlayer()) || Game.instance.isDead(event.getPlayer()))
            return;

        if (!Game.instance.canBreakBlock(event.getPlayer(), event.getBlock())) {
            if (Config.getFile().getBoolean(ConfigKey.ENABLE_GAME_MESSAGES.toString()))
                event.getPlayer().sendMessage(ChatColor.RED + Language.getTranslation(Language.TranslationKey.IN_GAME_BLOCK_BREAK));

            event.setCancelled(true);
            return;
        }

        event.setDropItems(false);
        event.setExpToDrop(0);
        Bukkit.getPluginManager().callEvent(new InGameBlockBroken(event.getPlayer(), event.getBlock().getLocation(), event.getBlock()));
    }

    @EventHandler
    public void playerPlaceBlock(final BlockPlaceEvent event) {
        if (event.isCancelled())
            return;

        if (Game.instance.isPlayerInGame(event.getPlayer())) {
            if (Config.getFile().getBoolean(ConfigKey.ENABLE_GAME_MESSAGES.toString()))
                event.getPlayer().sendMessage(ChatColor.RED + Language.getTranslation(Language.TranslationKey.IN_GAME_BLOCK_PLACING));
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void playerInteractEvent(final PlayerInteractEvent event) {
        if (!Game.instance.isPlayerInGame(event.getPlayer()) || event.getAction() == Action.LEFT_CLICK_BLOCK)
            return;

        event.setUseInteractedBlock(Event.Result.DENY);
        event.setUseItemInHand(Event.Result.DENY);
        event.setCancelled(true);

        if (event.getPlayer().getInventory().getItemInMainHand().equals(GameUtils.getInstance().getQuitItem()))
            Game.instance.removePlayer(event.getPlayer(), true);
    }

    @EventHandler
    public void playerDropItem(final PlayerDropItemEvent event) {
        if (event.isCancelled() || !Game.instance.isPlayerInGame(event.getPlayer()))
            return;

        event.setCancelled(true);
    }

    /**
     * Removes the player from the game should they change gamemode
     * Also clears their inventory
     * InGameLeave event is cancelled
     *
     * @param event
     * PlayerGameModeChangeEvent event launched by the server
     */
    @EventHandler
    public void playerChangedGamemode(final PlayerGameModeChangeEvent event) {
        if (event.isCancelled() || !Game.instance.isPlayerInGame(event.getPlayer()) || event.getNewGameMode() == GameMode.SURVIVAL)
            return;

        Player player = event.getPlayer();

        Game.instance.removePlayer(player, true);

        if (Config.getFile().getBoolean(ConfigKey.ENABLE_GAME_MESSAGES.toString()))
            player.sendMessage(Language.getTranslation(Language.TranslationKey.IN_GAME_GAMEMODE_CHANGED));

        player.getInventory().clear();
        player.setCanPickupItems(true);
        Game.instance.removePlayerScoreboard(player);
    }

    @EventHandler
    public void onPlayerDamaging(final EntityDamageByEntityEvent event) {
        if (event.isCancelled() || !(event.getEntity() instanceof Player) || !Game.instance.isPlayerInGame((Player) event.getEntity()))
            return;

        event.setCancelled(true);
    }

    @EventHandler
    public void onPlayerGoingToDie(final InGameLavaSpawn event) {
        Game.instance.clearMinedBlock(event.getPlayer());
        event.getPlayer().getInventory().clear();

        Game.instance.setDead(event.getPlayer());

        SoundData sound = (SoundData) Config.getFile().get(ConfigKey.SOUND_LAVA_SPAWN.toString());
        sound.play(event.getPlayer());
    }


    /************************************************************

                        Player health/Food

     ************************************************************/

    @EventHandler
    public void onPlayerTakingDamage(final EntityDamageEvent event) {
        if (event.isCancelled() || !(event.getEntity() instanceof Player) || !Game.instance.isPlayerInGame((Player) event.getEntity()))
            return;

        if (event.getCause() != EntityDamageEvent.DamageCause.LAVA)
            event.setCancelled(true);


        Player player = (Player) event.getEntity();

        if ((player.getHealth() - event.getFinalDamage()) <= 0) {
            event.setCancelled(true);
            player.setFoodLevel(20);
            player.setSaturation(20);
            if (Config.getFile().getBoolean(ConfigKey.ENABLE_GAME_MESSAGES.toString()))
                player.sendMessage(Language.getTranslation(Language.TranslationKey.IN_GAME_DIED));
            Game.instance.removePlayer(player, false);
        }
    }

    @EventHandler
    public void onPlayerLoosingFood(final FoodLevelChangeEvent event) {
        if (event.isCancelled() || !(event.getEntity() instanceof Player) || !Game.instance.isPlayerInGame((Player) event.getEntity()))
            return;

        event.setCancelled(true);
        event.getEntity().setFoodLevel(20);
        event.getEntity().setSaturation(20);
    }
}
