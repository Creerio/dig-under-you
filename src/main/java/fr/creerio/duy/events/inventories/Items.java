/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.events.inventories;

import fr.creerio.duy.modules.Config;
import fr.creerio.duy.modules.GameUtils;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.modules.SaveType;
import fr.creerio.duy.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Starting item inventory, can be modified
 */
public class Items implements Listener {
    private static final Inventory inv = Bukkit.createInventory(null, 9 , "Starting items");
    private static boolean isUsed = false;

    private static final ItemStack nullItem = Utils.getNullItem();

    public Items() {
        // Slot blocked from being modified
        inv.setItem(8, Utils.getNullItem());
    }

    /**
     * @return if the inventory is used
     */
    public static boolean isIsUsed() {
        return isUsed;
    }

    public static void openInventory(final Player player, final List<ItemStack> items) {
        isUsed = true;
        ItemsRead.openInventory(inv, player, items.stream().map(ItemStack::clone).collect(Collectors.toList()));
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryClick(final InventoryClickEvent event) {
        if (event.isCancelled() || !event.getInventory().equals(inv) || event.getCurrentItem() == null)
            return;

        // No effect when clicking on the blocked slot
        if (event.getCurrentItem().equals(nullItem))
            event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryDrag(final InventoryDragEvent event) {
        if (event.isCancelled() || !event.getInventory().equals(inv) || event.getCursor() == null)
            return;

        // No effect when clicking on the blocked slot
        if (event.getCursor().equals(nullItem))
            event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryClose(final InventoryCloseEvent event) {
        if (!event.getInventory().equals(inv))
            return;

        isUsed = false;

        // Fetch every item and save them
        List<ItemStack> newItems = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            if (inv.getItem(i) != null)
                newItems.add(inv.getItem(i).clone());
        }
        GameUtils.getInstance().updateStartingItems(newItems);

        event.getPlayer().sendMessage(Language.getTranslation(Language.TranslationKey.ITEMS_GUI_SAVED));
        Utils.playSuccessSound((Player) event.getPlayer());

        if (SaveType.getType(Config.getFile().getString(Config.ConfigKey.SAVE_TYPE.toString())) == SaveType.ON_UPDATE)
            Config.save();
    }
}
