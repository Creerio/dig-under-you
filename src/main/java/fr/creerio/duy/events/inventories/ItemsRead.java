/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.events.inventories;

import fr.creerio.duy.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Starting item inventory, READ ONLY GUI
 */
public class ItemsRead implements Listener {
    private static final Map<HumanEntity, Inventory> inv = new HashMap<>();

    public static void openInventory(final Player player, final List<ItemStack> items) {
        Inventory plInv = Bukkit.createInventory(null, 9 , "Starting items");
        inv.put(player, plInv);
        plInv.setItem(8, Utils.getNullItem());
        openInventory(plInv, player, items);
    }

    protected static void openInventory(final Inventory inv, final Player player, final List<ItemStack> items) {
        for (int i = 0; i < 8; i++) {
            inv.setItem(i, i < items.size() ? items.get(i) : null);
        }
        player.closeInventory();
        player.openInventory(inv);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryClick(final InventoryClickEvent event) {
        if (event.isCancelled() || !inv.containsKey(event.getWhoClicked()) || !event.getInventory().equals(inv.get(event.getWhoClicked())))
            return;

        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryDrag(final InventoryDragEvent event) {
        if (event.isCancelled() || !inv.containsKey(event.getWhoClicked()) || !event.getInventory().equals(inv.get(event.getWhoClicked())))
            return;

        event.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onInventoryClose(final InventoryCloseEvent event) {
        if (!inv.containsKey(event.getPlayer()) || !event.getInventory().equals(inv.get(event.getPlayer())))
            return;

        inv.remove(event.getPlayer());
    }
}
