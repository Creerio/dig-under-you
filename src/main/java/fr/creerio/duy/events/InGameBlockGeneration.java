/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.events;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Event used when a block is generated
 */
public class InGameBlockGeneration extends Event implements Cancellable {

    /**
     * Handlers required by spigot itself
     */
    private static final HandlerList HANDLERS = new HandlerList();

    /**
     * Player under which the block is generated
     */
    private final Player player;

    /**
     * Location where the block is generated
     */
    private final Location location;

    /**
     * If the event is cancelled
     */
    private boolean isCancelled = false;

    /**
     * Event constructor
     * Clones the given location
     *
     * @param pl
     * Player used by this event
     *
     * @param loc
     * Location used by this event
     */
    public InGameBlockGeneration(Player pl, Location loc) {
        player = pl;
        location = loc.clone();
    }

    public final Player getPlayer() {
        return player;
    }

    public final Location getLocation() {
        return location;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

    @Override
    public HandlerList getHandlers() {
        return getHandlerList();
    }

    @Override
    public boolean isCancelled() {
        return isCancelled;
    }

    @Override
    public void setCancelled(boolean cancel) {
        isCancelled = cancel;
    }
}
