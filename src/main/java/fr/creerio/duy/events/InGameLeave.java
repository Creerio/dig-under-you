/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.events;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.Map;

/**
 * Event used when a player leaves the game
 * Can be cancelled immediately should a player disconnect or if an InGameJoin event is cancelled (in case you don't want the user to join the game), must be handled !
 */
public class InGameLeave extends Event implements Cancellable {

    /**
     * Handlers required by spigot itself
     */
    private static final HandlerList HANDLERS = new HandlerList();

    /**
     * Player being added in game
     */
    private final Player player;

    /**
     * Location used to put back blocks on the joined player position
     */
    private final Location location;

    /**
     * If the player survived
     */
    private final boolean survived;

    /**
     * Blocks broken by the player
     */
    private final Map<Material, Integer> minedBlocks;

    /**
     * If the event is cancelled
     */
    private boolean isCancelled = false;

    /**
     * Event constructor
     * Clones the given location
     *
     * @param pl
     * Player used by this event
     */
    public InGameLeave(final Player pl, final Location loc, final boolean alive, final Map<Material, Integer> mined) {
        player = pl;
        location = loc;
        survived = alive;
        minedBlocks = mined;
    }

    public final Player getPlayer() {
        return player;
    }

    public final Location getLocation() {
        return location;
    }

    public final boolean getSurvived() {
        return survived;
    }

    public final Map<Material, Integer> getMinedBlocks() {
        return minedBlocks;
    }

    public static HandlerList getHandlerList() {
        return HANDLERS;
    }

    @Override
    public HandlerList getHandlers() {
        return getHandlerList();
    }

    @Override
    public boolean isCancelled() {
        return isCancelled;
    }

    @Override
    public void setCancelled(boolean cancel) {
        isCancelled = cancel;
    }
}
