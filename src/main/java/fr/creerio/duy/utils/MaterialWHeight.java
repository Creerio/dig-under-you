/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.utils;

import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import java.util.HashMap;
import java.util.Map;

public class MaterialWHeight implements ConfigurationSerializable{

    private final Material mtr;

    private final int height;

    public MaterialWHeight(final Material material, final int hgt) {
        mtr = material;
        height = hgt;
    }

    public Material getMaterial() {
        return mtr;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> data = new HashMap<>();

        data.put("block", mtr.toString());
        data.put("height", height);

        return data;
    }

    public static MaterialWHeight deserialize(Map<String, Object> args) {
        return new MaterialWHeight(Material.getMaterial((String) args.get("block")), (Integer) args.get("height"));
    }

    @Override
    public String toString() {
        return "MaterialWHeight{" +
                "block=" + mtr +
                ", height=" + height +
                '}';
    }
}
