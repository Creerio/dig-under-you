/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.utils;

import org.bukkit.Sound;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.entity.Player;
import org.bukkit.util.NumberConversions;

import java.util.HashMap;
import java.util.Map;

/**
 * Serializable & complete sound (with volume and pitch)
 * Also contains a play method for ease of use
 */
public class SoundData implements ConfigurationSerializable {

    private final Sound sound;

    private final float volume;

    private final float pitch;

    public SoundData(final Sound snd, final float vlm, final float ptch) {
        sound = snd;

        if (vlm < 0)
            volume = 0;
        else if (vlm > 1)
            volume = 1;
        else
            volume = vlm;

        if (ptch < 0)
            pitch = 0;
        else if (ptch > 2)
            pitch = 2;
        else
            pitch = ptch;
    }

    public Sound getSound() {
        return sound;
    }

    public float getVolume() {
        return volume;
    }

    public float getPitch() {
        return pitch;
    }

    public void play(final Player player) {
        player.playSound(player.getLocation(), sound, volume, pitch);
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> data = new HashMap<>();

        data.put("sound", sound.toString());
        data.put("volume", volume);
        data.put("pitch", pitch);

        return data;
    }

    public static SoundData deserialize(Map<String, Object> args) {
        return new SoundData(Sound.valueOf((String) args.get("sound")), NumberConversions.toFloat(args.get("volume")), NumberConversions.toFloat(args.get("pitch")));
    }

    @Override
    public String toString() {
        return sound.toString() + " with volume " + volume + " and pitch " + pitch;
    }
}
