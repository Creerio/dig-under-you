/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.utils.commands;

import org.bukkit.command.CommandSender;

import java.util.List;

public interface SubCommandTabCompleter {
    /**
     * @param sender
     * Initiator of the subcommand
     *
     * @param args
     * Arguments given by the sender while writing the command
     *
     * @return What the sender can use
     */
    List<String> onTabComplete(CommandSender sender, String[] args);
}
