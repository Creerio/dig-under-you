/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.utils.commands;

import fr.creerio.duy.modules.Language;
import fr.creerio.duy.utils.Utils;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.*;
import java.util.stream.Collectors;

public class SubCommandContainer {
    /**
     * Subcommands can also contain subcommands themselves
     */
    protected HashMap<String, SubCommandBase> subCommands = new HashMap<>();

    /**
     * Used to add subcommands to the current command
     *
     * @param subCmds
     * Subcommands to add
     */
    protected void addSubCommands(SubCommandBase ... subCmds) {
        Arrays.stream(subCmds).collect(Collectors.toList()).forEach(subCommand -> subCommands.put(subCommand.getName(), subCommand));
    }

    protected boolean onCommandHandleSub(CommandSender sender, String[] args) {
        if (args.length >= 1) {
            SubCommandBase targetedSubCmd = subCommands.get(args[0]);

            if (!targetedSubCmd.isAllowed(sender))
                return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.MISSING_PERMISSIONS));

            if (targetedSubCmd instanceof SubCommandExecutor)
                return ((SubCommandExecutor) targetedSubCmd).onSubCommand(sender, SubCommandBase.subCommandParser(args));
        }

        sender.sendMessage("Available subcommands : ");
        subCommands.forEach((s, subCommandBase) -> {
            sender.sendMessage("  - \"" + subCommandBase.getName() + "\" => " + subCommandBase.getDescription() + "\n");
            sender.sendMessage(ChatColor.RED + "  /" + subCommandBase.getSyntax());
        });
        return true;
    }

    protected List<String> onTabCompleteHandleSub(CommandSender sender, String[] args) {
        if (args.length == 0)
            return Collections.emptyList();

        SubCommandBase targetedSubCmd = subCommands.get(args[0]);

        if (targetedSubCmd instanceof SubCommand)
            return ((SubCommand) targetedSubCmd).onTabComplete(sender, SubCommandBase.subCommandParser(args));

        if (args.length == 1) {
            if (!args[0].isEmpty())
                return subCommands.entrySet().stream().filter(entry -> entry.getKey().startsWith(args[0]) && entry.getValue().isAllowed(sender)).map(Map.Entry::getKey).collect(Collectors.toList());
            else
                return subCommands.entrySet().stream().filter(entry -> entry.getValue().isAllowed(sender)).map(Map.Entry::getKey).collect(Collectors.toList());
        }

        return Collections.emptyList();
    }
}
