/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.util.NumberConversions;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * A serializable location object
 * Used to save properly locations inside the config file (since the default location uses the world's name which causes issues)
 */
public class LocationSerializable implements ConfigurationSerializable {

    private static final String WORLD_STR = "world";

    private final World world;
    private double x;
    private double y;
    private double z;
    private float pitch;
    private float yaw;

    private LocationSerializable(World world, double x, double y, double z, float pitch, float yaw) {
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
        this.pitch = pitch;
        this.yaw = yaw;
    }

    public LocationSerializable(Location loc) {
        world = loc.getWorld();
        x = loc.getX();
        y = loc.getY();
        z = loc.getZ();
        pitch = loc.getPitch();
        yaw = loc.getYaw();
    }

    /**
     * Removes digits from the location, then center to the block
     */
    public void normalizePosition() {
        x = Location.locToBlock(x) + 0.5;
        y = Location.locToBlock(y);
        z = Location.locToBlock(z) + 0.5;
    }

    public void removeOrientation() {
        pitch = 0f;
        yaw = 0f;
    }

    public Location toLocation() {
        return new Location(world, x, y, z, yaw, pitch);
    }


    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> data = new HashMap<>();

        if (this.world != null)
            data.put(WORLD_STR, world.getUID().toString());


        data.put("x", x);
        data.put("y", y);
        data.put("z", z);

        data.put("yaw", yaw);
        data.put("pitch", pitch);

        return data;
    }

    public static LocationSerializable deserialize(Map<String, Object> args) {
        World world = null;
        if (args.containsKey(WORLD_STR)) {
            world = Bukkit.getWorld(UUID.fromString((String) args.get(WORLD_STR)));
            if (world == null)
                throw new IllegalArgumentException("unknown world");

        }

        return new LocationSerializable(world, NumberConversions.toDouble(args.get("x")), NumberConversions.toDouble(args.get("y")), NumberConversions.toDouble(args.get("z")), NumberConversions.toFloat(args.get("yaw")), NumberConversions.toFloat(args.get("pitch")));
    }
}
