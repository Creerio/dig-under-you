/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.utils;

import fr.creerio.duy.modules.Config;
import fr.creerio.duy.modules.Language;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Utils {

    private Utils() {}

    /**
     * Sends an error message to the user
     * Also plays a sound if they are a player
     *
     * @param receiver
     * Receiver of the error message
     *
     * @param msg
     * Error message
     *
     * @return false
     */
    public static boolean sendErrorMsg(final CommandSender receiver, final String msg) {
        receiver.sendMessage(ChatColor.RED + Language.getTranslation(Language.TranslationKey.ERROR) + msg);

        if (receiver instanceof Player) {
            playErrorSound((Player) receiver);
        }

        return false;
    }

    public static boolean sendMaintenanceMsg(final CommandSender receiver) {
        return sendErrorMsg(receiver, Language.getTranslation(Language.TranslationKey.MAINTENANCE_BLOCK));
    }

    public static boolean sendMaintenanceRequirementMsg(final CommandSender receiver) {
        return sendErrorMsg(receiver, Language.getTranslation(Language.TranslationKey.MAINTENANCE_REQUIRED));
    }

    public static void playSuccessSound(final Player player) {
        if (Config.getFile().getBoolean(Config.ConfigKey.SOUND_ENABLED.toString()) && Config.getFile().getString(Config.ConfigKey.SOUND_SUCCESS.toString()) != null) {
            SoundData sound = (SoundData) Config.getFile().get(Config.ConfigKey.SOUND_SUCCESS.toString());
            sound.play(player);
        }
    }

    public static void playErrorSound(final Player player) {
        if (Config.getFile().getBoolean(Config.ConfigKey.SOUND_ENABLED.toString()) && Config.getFile().getString(Config.ConfigKey.SOUND_ERROR.toString()) != null) {
            SoundData sound = (SoundData) Config.getFile().get(Config.ConfigKey.SOUND_ERROR.toString());
            sound.play(player);
        }
    }

    /**
     * @return A red glass pane with no name
     */
    public static ItemStack getNullItem() {
        final ItemStack nullItem = new ItemStack(Material.RED_STAINED_GLASS_PANE);
        final ItemMeta nullItemMeta = nullItem.getItemMeta();

        nullItemMeta.setDisplayName("§r");

        nullItem.setItemMeta(nullItemMeta);
        return nullItem;
    }

    /**
     * @param sender
     * The person who tried to send the integer
     *
     * @param nb
     * The potential integer read
     *
     * @return Either the value or {@link Integer#MIN_VALUE Integer.MIN_VALUE} when it fails to read it
     */
    public static int getInt(final CommandSender sender, final String nb) {
        int res = Integer.MIN_VALUE;
        try {
            res = Integer.parseInt(nb);
        }
        catch (NumberFormatException e) {
            Bukkit.getLogger().severe(e.toString());
            sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.FAILED_READ_NUMBER));
        }
        return res;
    }
}
