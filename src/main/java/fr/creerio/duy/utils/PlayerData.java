/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.utils;

import org.bukkit.Location;
import org.bukkit.Material;

import java.util.HashMap;
import java.util.Map;

public class PlayerData {

    private final Location location;
    private boolean dead = false;

    private final Map<Material, Integer> minedBlocks = new HashMap<>();

    public PlayerData(Location loc) {
        location = loc;
    }

    public Location getLocation() {
        return location;
    }

    public void addMinedBlock(Material mtr) {
        minedBlocks.merge(mtr, 1, Integer::sum);
    }

    public Map<Material, Integer> getMinedBlocks() {
        return minedBlocks;
    }

    public boolean isDead() {
        return dead;
    }

    public void setDead() {
        dead = true;
    }

    public void clearMinedBlocks() {
        minedBlocks.clear();
    }
}
