/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.modules;

import fr.creerio.duy.DUY;
import fr.creerio.duy.modules.Config.ConfigKey;
import fr.creerio.duy.utils.MaterialWHeight;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Class for useful game elements (block types, items)
 */
public class GameUtils {

    private static final List<Material> topBlocks = new ArrayList<>();

    private static final Map<Integer, List<Material>> bottomBlocks = new HashMap<>();
    private static final List<MaterialWHeight> bottomBlockList = new ArrayList<>();

    private static final List<ItemStack> startingItems = new ArrayList<>();

    public static final String ILLEGAL_BLOCK_TYPES = "^((.*(BUTTON|DOOR|FENCE|PRESSURE_PLATE|SAPPLING|TRAPDOOR|STEM|BANNER|SIGN|RAIL).*)|AIR)$";

    private static GameUtils instance = null;

    /**
     * Singleton object, no access is provided by default
     */
    private GameUtils() {
        List<String> topBlocksList = Config.getFile().getStringList(ConfigKey.TOP_BLOCKS_TAG.toString());
        List<MaterialWHeight> bottomBlocksList = (List<MaterialWHeight>) Config.getFile().getList(ConfigKey.BOTTOM_BLOCKS_TAG.toString());
        List<ItemStack> startingItms = (List<ItemStack>) Config.getFile().getList(ConfigKey.STARTING_ITEMS_TAG.toString());

        topBlocksList.forEach(materialStr -> {
            Material mtr = Material.getMaterial(materialStr);
            if (mtr == null || !isValidMaterial(mtr))
                Bukkit.getLogger().warning(Language.getTranslation(Language.TranslationKey.INVALID_STARTUP_TOP_BLOCK, materialStr));
            else
                topBlocks.add(mtr);
        });

        assert bottomBlocksList != null;
        bottomBlocksList.forEach(mtr -> {
            if (!isValidMaterial(mtr.getMaterial()))
                Bukkit.getLogger().warning(Language.getTranslation(Language.TranslationKey.INVALID_STARTUP_BOTTOM_BLOCK, mtr.toString()));
            else {
                int height = mtr.getHeight();

                if (!bottomBlocks.containsKey(height))
                    bottomBlocks.put(height, new ArrayList<>());
                if (!bottomBlocks.get(height).contains(mtr.getMaterial())) {
                    bottomBlocks.get(height).add(mtr.getMaterial());
                    bottomBlockList.add(mtr);
                }
            }
        });

        assert startingItms != null;
        for (int i = 0; i < startingItms.size(); i++) {
            if (i >= 8) {
                String startingItem = Language.getTranslation(Language.TranslationKey.INVALID_STARTUP_TOO_MUCH_ITEMS, String.valueOf(i),  startingItms.get(i).toString());
                Bukkit.getLogger().warning(startingItem);
            }
            startingItems.add(startingItms.get(i));
        }
    }

    /**
     * @return GameUtils instance
     */
    public static GameUtils getInstance() {
        if (instance == null)
            instance = new GameUtils();

        return instance;
    }

    /**
     * Reloads block data and items from config file
     * DO NOT USE IF EVERYTHING WORKS FINE, IT IS INTENDED FOR RELOADING THE CONFIG FILE !
     */
    public static void reloadInstance() {
        topBlocks.clear();
        bottomBlocks.values().forEach(List::clear);
        bottomBlockList.clear();
        startingItems.clear();

        instance = new GameUtils();
    }


    /**
     * Returns if the provided material is valid, in this case we expect a block (though not everything is handled)
     *
     * @param mtr
     * Material to check
     *
     * @return true if it is a block and not an {@link GameUtils#ILLEGAL_BLOCK_TYPES illegal type}, else false
     */
    public static boolean isValidMaterial(final Material mtr) {
        return (mtr.isBlock() && !Pattern.matches(ILLEGAL_BLOCK_TYPES, mtr.toString()));
    }

    /**
     * Adds new given material as a top block
     *
     * @param topBlock
     * New top block material
     *
     * @return false if the material is already stored or is it's not a block, else will always return true (as specified by {@link java.util.Collection#add(Object) Collection.add(E)})
     */
    public final boolean addTopBlock(final Material topBlock) {
        if (!isValidMaterial(topBlock) || topBlocks.contains(topBlock))
            return false;

        return topBlocks.add(topBlock);
    }

    /**
     * Adds new given material as a bottom block
     *
     * @param bottomBlock
     * New bottom block
     *
     * @return false if the material is already stored with the same height or is it's not a block, else will always return true
     */
    public final boolean addBottomBlock(final MaterialWHeight bottomBlock) {
        if (!isValidMaterial(bottomBlock.getMaterial()) || (bottomBlocks.get(bottomBlock.getHeight()) != null && bottomBlocks.get(bottomBlock.getHeight()).contains(bottomBlock.getMaterial())))
            return false;

        int height = bottomBlock.getHeight();

        // Add to ordered storage & non-ordered one
        bottomBlocks.computeIfAbsent(height, ht -> bottomBlocks.put(ht, new ArrayList<>()));
        bottomBlocks.get(height).add(bottomBlock.getMaterial());

        bottomBlockList.add(bottomBlock);

        return true;
    }

    /**
     * Adds given itemstack, cloned (hopefully an item w/ durability &/or enchantments)
     *
     * @param item
     * ItemStack to add
     *
     * @return false if already stored, else will always return true (as specified by {@link java.util.Collection#add(Object) Collection.add(E)})
     */
    public final boolean addStartingItem(final ItemStack item) {
        ItemStack newItem = item.clone();
        if (startingItems.contains(newItem))
            return false;

        return startingItems.add(newItem);
    }

    /**
     * Removes the given top block material from the stored ones
     *
     * @param topBlock
     * Material to remove
     *
     * @return false if not stored, else true
     */
    public final boolean removeTopBlock(final Material topBlock) {
        int pos = topBlocks.indexOf(topBlock);
        if (pos == -1)
            return false;

        return topBlocks.remove(pos) != null;
    }

    /**
     * Removes a bottom block from the stored ones
     *
     * @param bottomBlockPos
     * Position of the block to remove
     *
     * @return false if not stored, else true
     */
    public final boolean removeBottomBlock(final int bottomBlockPos) {
        if (bottomBlockPos < 0 || bottomBlockList.size() <= bottomBlockPos)
            return false;

        MaterialWHeight bottomBlock = bottomBlockList.remove(bottomBlockPos);
        if (bottomBlock == null)
            return false;

        bottomBlocks.get(bottomBlock.getHeight()).remove(bottomBlock.getMaterial());

        return true;
    }

    /**
     * Removes given itemstack
     *
     * @param pos
     * Position of the itemstack to remove
     *
     * @return false if not stored, else true
     */
    public final boolean removeStartingItem(final int pos) {
        if (pos < 0 || startingItems.size() <= pos)
            return false;

        return startingItems.remove(pos) != null;
    }

    public void updateStartingItems(final List<ItemStack> items) {
        startingItems.clear();
        startingItems.addAll(items);
    }

    /**
     * @return Either null (no blocks stored) or any topBlock
     */
    public final Material randomTopBlock() {
        if (topBlocks.isEmpty())
            return null;

        return topBlocks.size() == 1 ? topBlocks.get(0) : topBlocks.get(ThreadLocalRandom.current().nextInt(topBlocks.size()));
    }

    /**
     * @return Either null (no blocks stored) or any bottomBlock
     */
    public final Material randomBottomBlock(int height) {
        if (bottomBlockList.isEmpty())
            return null;

        List<Material> blocks = new ArrayList<>();
        bottomBlocks.forEach((k, v) -> {
            if (k >= height)
                blocks.addAll(v);
        });

        if (blocks.isEmpty())
            return Material.AIR;

        return blocks.size() == 1 ? blocks.get(0) : blocks.get(ThreadLocalRandom.current().nextInt(blocks.size()));
    }

    /**
     * @return an arraylist containing the top blocks
     */
    public final List<Material> listTopBlocks() {
        return new ArrayList<>(topBlocks);
    }

    /**
     * @return an arraylist containing the bottom blocks
     */
    public final List<MaterialWHeight> listBottomBlocks() {
        return new ArrayList<>(bottomBlockList);
    }

    /**
     * @return a clone of the starting items
     */
    public final List<ItemStack> getStartingItems() {
        return startingItems.stream().map(ItemStack::clone).collect(Collectors.toList());
    }

    public final int numberOfStartingItems() {
        return startingItems.size();
    }

    /**
     * Checks if the given material block can be broken (belongs to the bottomBlocks list)
     *
     * @param material
     * Block's material broken by the player
     *
     * @return true if it's in the bottomBlocks list, else false
     */
    public final boolean canBeBroken(Material material) {
        return bottomBlockList.stream().map(MaterialWHeight::getMaterial).anyMatch(blk -> blk.equals(material)) || topBlocks.contains(material);
    }

    /**
     * @return if blocks are defined as top & bottom
     */
    public final boolean isUsable() {
        return !topBlocks.isEmpty() && !bottomBlocks.isEmpty();
    }

    /**
     * @return A serializable list of top blocks
     */
    public final List<String> getSerializableTopBlocks() {
        return topBlocks.stream().map(Enum::toString).collect(Collectors.toList());
    }

    /**
     * @return A serializable list of bottom blocks
     */
    public final List<MaterialWHeight> getSerializableBottomBlocks() {
        return bottomBlockList;
    }

    public final ItemStack getQuitItem() {
        ItemStack quitItem = new ItemStack(Material.RED_TERRACOTTA);
        ItemMeta quitItemMeta = quitItem.getItemMeta();

        quitItemMeta.setDisplayName(ChatColor.RED + "Quit");
        quitItemMeta.getPersistentDataContainer().set(DUY.getPluginKey(), PersistentDataType.STRING, DUY.TAG);

        quitItem.setItemMeta(quitItemMeta);
        return quitItem;
    }
}
