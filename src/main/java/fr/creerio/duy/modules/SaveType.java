/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.modules;

/**
 * Saving types usable
 */
public enum SaveType {

    MANUAL("manual"),
    PERIODIC("periodic"),
    ON_UPDATE("on_update");

    private final String type;

    SaveType(String tpe) {
        type = tpe;
    }

    public static SaveType getType(String data) {
        switch (data) {
            case "manual":
                return MANUAL;
            case "periodic":
                return PERIODIC;
            case "on_update":
                return ON_UPDATE;
            default:
                return null;
        }
    }

    @Override
    public String toString() {
        return type;
    }
}
