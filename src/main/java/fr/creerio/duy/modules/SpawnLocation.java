/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.modules;

import fr.creerio.duy.modules.Config.ConfigKey;
import fr.creerio.duy.utils.LocationSerializable;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SpawnLocation {

    private final List<Location> spawnLocations = new ArrayList<>();

    private static SpawnLocation instance = null;

    /**
     * Singleton object, no access is provided by default
     */
    private SpawnLocation() {
        List<LocationSerializable> locs = (List<LocationSerializable>) Config.getFile().getList(ConfigKey.SPAWN_LOCATIONS_TAG.toString());

        assert locs != null;

        if (Config.getFile().getInt(ConfigKey.MAX_HEIGHT_TAG.toString()) < 0) {
            Bukkit.getLogger().warning(() -> Language.getTranslation(Language.TranslationKey.INVALID_STARTUP_HEIGHT));
            Config.getFile().set(ConfigKey.MAX_HEIGHT_TAG.toString(), 0);
        }

        for (LocationSerializable loc : locs) {
            Location aLoc = loc.toLocation();
            if (!validHeight(aLoc)) {
                String locStr = Language.getTranslation(Language.TranslationKey.LOCATION, String.valueOf(aLoc.getX()), String.valueOf(aLoc.getY()), String.valueOf(aLoc.getZ()), (aLoc.getWorld() != null ? aLoc.getWorld().getName() : null));
                Bukkit.getLogger().warning(() -> Language.getTranslation(Language.TranslationKey.INVALID_STARTUP_LOCATION, locStr));
            }
            else
                spawnLocations.add(aLoc);
        }
    }

    /**
     * @return Spawn location instance
     */
    public static SpawnLocation getInstance() {
        if (instance == null)
            instance = new SpawnLocation();

        return instance;
    }

    /**
     * Reloads spawn location from config file
     * DO NOT USE IF EVERYTHING WORKS FINE, IT IS INTENDED FOR RELOADING THE CONFIG FILE !
     */
    public static void reloadInstance() {
        instance = new SpawnLocation();
    }

    /**
     * @return A copy of the currently stored spawn locations (cloned for security)
     */
    public List<Location> get() {
        return spawnLocations.stream().map(Location::clone).collect(Collectors.toList());
    }

    /**
     * @return Serializable locations, for the config file
     */
    public List<LocationSerializable> getSerialisableLocations() {
        List<LocationSerializable> locs = new ArrayList<>();
        for (Location loc : spawnLocations)
            locs.add(new LocationSerializable(loc));

        return locs;
    }

    /**
     * Adds new given location
     *
     * @param newLocation
     * New location to add
     *
     * @return false if the location is already stored, else will always return true (as specified by {@link java.util.Collection#add(Object) Collection.add(E)})
     */
    public boolean addLocation(Location newLocation) {
        if (spawnLocations.contains(newLocation))
            return false;

        return spawnLocations.add(newLocation);
    }

    /**
     * Removes the given location from the stored ones
     *
     * @param location
     * Location to remove
     *
     * @return false if not stored, else true
     */
    public boolean removeLocation(Location location) {
        int pos = spawnLocations.indexOf(location);
        if (pos == -1)
            return false;

        return spawnLocations.remove(pos) != null;
    }

    /**
     * Used to check a location's height against the currently used height value
     * Uses {@link fr.creerio.duy.modules.SpawnLocation#validHeight(Location, int) SpawnLocation.validHeight(Location, int)}
     *
     * @param location
     * Location to verify
     *
     * @return true if the location is between the bounds of the world (with at least one block to spare), false if not
     */
    public boolean validHeight(Location location) {
        return validHeight(location, Config.getFile().getInt(ConfigKey.MAX_HEIGHT_TAG.toString()));
    }

    /**
     * Used to check a location's height
     *
     * @param location
     * Location to verify
     *
     * @param height
     * Height used against location
     *
     * @return true if the location is between the bounds of the world (with at least one block to spare), false if not
     */
    public boolean validHeight(Location location, int height) {
        int minHeight = location.getWorld().getMinHeight();
        return location.getBlockY() - height >= minHeight;
    }

    /**
     * Checks if all stored heights are compatible with the one provided
     *
     * @param height
     * Height provided
     *
     * @return true if everything is fine, else false
     */
    public boolean isHeightCompatible(int height) {
        for (Location loc : spawnLocations) {
            if (!validHeight(loc, height))
                return false;
        }
        return true;
    }
}
