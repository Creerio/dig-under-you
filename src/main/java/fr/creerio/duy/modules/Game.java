/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.modules;

import fr.creerio.duy.DUY;
import fr.creerio.duy.modules.Config.ConfigKey;
import fr.creerio.duy.events.InGameJoin;
import fr.creerio.duy.events.InGameLavaSpawn;
import fr.creerio.duy.events.InGameLeave;
import fr.creerio.duy.utils.PlayerData;
import fr.creerio.duy.utils.SoundData;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.BaseComponent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

public class Game {

    private final Map<Player, PlayerData> inGamePlayers = new HashMap<>();

    /**
     * Singleton object
     */
    public static final Game instance = new Game();

    private Game() {}

    /**
     * Tries to add a player
     *
     * @param player
     * Player joining the game
     *
     * @return SUCCESS or a GameResponse describing the issue
     */
    public final GameResponse addPlayer(final Player player) {
        if (isPlayerInGame(player))
            return GameResponse.IN_GAME;

        List<Location> locs = SpawnLocation.getInstance().get();
        locs.removeAll(inGamePlayers.values().stream().map(PlayerData::getLocation).collect(Collectors.toList()));

        if (locs.isEmpty())
            return GameResponse.NO_LOCATION_AVAILABLE;

        Location randomLoc = locs.size() == 1 ? locs.get(0) : locs.get(ThreadLocalRandom.current().nextInt(locs.size()));
        inGamePlayers.put(player, new PlayerData(randomLoc.clone()));

        // Launch join event, allowing for the server to catch and cancel if they want to
        Bukkit.getPluginManager().callEvent(new InGameJoin(player, randomLoc));

        return GameResponse.SUCCESS;
    }

    /**
     * Tries to remove a player
     *
     * @param player
     * Player leaving the game
     *
     * @return SUCCESS or a GameResponse describing the issue
     */
    public final GameResponse removePlayer(final Player player, final boolean survived) {
        return removePlayer(player, survived, false);
    }

    /**
     * Tries to remove a player
     * Can cancel the remove event should it be needed
     *
     * @param player
     * Player leaving the game
     *
     * @param cancelEvent
     * If the event should be canceled
     *
     * @return SUCCESS or a GameResponse describing the issue
     */
    public final GameResponse removePlayer(final Player player, final boolean survived, final boolean cancelEvent) {
        if (!isPlayerInGame(player))
            return GameResponse.NOT_IN_GAME;

        PlayerData playerDt = inGamePlayers.get(player);

        inGamePlayers.remove(player);
        Game.instance.generateTopBlocks(playerDt.getLocation().clone());

        InGameLeave event = new InGameLeave(player, playerDt.getLocation().clone(), survived, playerDt.getMinedBlocks());
        event.setCancelled(cancelEvent);
        Bukkit.getPluginManager().callEvent(event);

        return GameResponse.SUCCESS;
    }

    public final List<Player> playersInGame() {
        return new ArrayList<>(inGamePlayers.keySet());
    }

    public final boolean isPlayerInGame(final Player player) {
        return inGamePlayers.containsKey(player);
    }

    public final boolean isEmpty() {
        return inGamePlayers.isEmpty();
    }

    public void addMinedBlock(final Player player, final Material mtr) {
        inGamePlayers.get(player).addMinedBlock(mtr);
    }

    public void clearMinedBlock(final Player player) {
        inGamePlayers.get(player).clearMinedBlocks();
    }

    public boolean isDead(final Player player) {
        return inGamePlayers.get(player).isDead();
    }

    public void setDead(final Player player) {
        inGamePlayers.get(player).setDead();
    }

    /**
     * Checks & fixes the player's location
     *
     * @param player
     * InGame player
     *
     * @param location
     * InGame player location
     */
    public void checkLocation(final Player player, final Location location) {
        Location loc = inGamePlayers.get(player).getLocation();

        if (location.getX() < loc.getX() || loc.getX() < location.getX())
            location.setX(loc.getX());

        if (location.getZ() < loc.getZ() || loc.getZ() < location.getZ())
            location.setZ(loc.getZ());
    }

    /**
     * Returns if the given block can be broken
     *
     * @param player
     * Player breaking the block
     *
     * @param block
     * Block broken
     *
     * @return true or false
     */
    public final boolean canBreakBlock(final Player player, final Block block) {
        Location loc = inGamePlayers.get(player).getLocation();

        return block.getX() == loc.getBlockX() && block.getZ() == loc.getBlockZ() && GameUtils.getInstance().canBeBroken(block.getType());
    }

    /**
     * Uses given height to define if lava will be generated
     *
     * @param height
     * Current height
     *
     * @return
     */
    private boolean checkLavaGenerate(final int height) {
        return ThreadLocalRandom.current().nextInt(height) == 0; // Can be improved for more variety
    }

    /**
     * Used to generate one block the player's position
     *
     * @param loc
     * Location of the mined block
     */
    public void generateBottomBlock(final Player player, final Location loc) {
        int lavaStartHeight = Config.getFile().getInt(ConfigKey.LAVA_START_TAG.toString());
        int height = Config.getFile().getInt(ConfigKey.MAX_HEIGHT_TAG.toString());
        int placeLoc = loc.getBlockY() - 1;
        int endAt = inGamePlayers.get(player).getLocation().getBlockY() - height;

        boolean putLava = placeLoc <= lavaStartHeight && checkLavaGenerate(placeLoc - endAt + 1);

        loc.setY(placeLoc);
        loc.getBlock().setType(putLava ? Material.LAVA : GameUtils.getInstance().randomBottomBlock(loc.getBlockY()));

        if (putLava) {
            Bukkit.getPluginManager().callEvent(new InGameLavaSpawn(player, loc));

            while (loc.getY() > endAt) {
                loc.setY(loc.getY() - 1);
                loc.getBlock().setType(Material.LAVA);
            }
        }

    }

    /**
     * Used to generate blocks under the starting location
     *
     * @deprecated FOR TESTING ONLY
     *
     * @param loc
     * Location from where the player starts
     */
    @Deprecated
    public void generateBottomBlocks(final Location loc) {
        if (GameUtils.getInstance().listBottomBlocks().isEmpty())
            return;

        int height = Config.getFile().getInt(ConfigKey.MAX_HEIGHT_TAG.toString());
        int lavaStartHeight = Config.getFile().getInt(ConfigKey.LAVA_START_TAG.toString());
        int startFrom = loc.getBlockY() - 1;
        int endAt = startFrom - (height - 1);

        boolean putLava = false;

        for (int i = startFrom; i > endAt; i--) {
            if (!putLava && i <= lavaStartHeight && checkLavaGenerate(height))
                putLava = true;

            loc.setY(i);
            loc.getBlock().setType(putLava ? Material.LAVA : GameUtils.getInstance().randomBottomBlock(loc.getBlockY()));
        }

        loc.setY(loc.getY() - 1);
        loc.getBlock().setType(Material.LAVA);
    }

    /**
     * Used to generate blocks under the starting location
     *
     * @param loc
     * Location from where the player starts
     */
    public void generateTopBlocks(final Location loc) {
        if (GameUtils.getInstance().listTopBlocks().isEmpty())
            return;

        int height = Config.getFile().getInt(ConfigKey.MAX_HEIGHT_TAG.toString());
        int endAt = loc.getBlockY() - (height + 1);

        loc.setY(loc.getY() - 1);
        loc.getBlock().setType(GameUtils.getInstance().randomTopBlock());

        for (int i = loc.getBlockY() - 1; i > endAt; i--) {
            loc.setY(i);
            loc.getBlock().setType(Material.STONE);
        }
    }

    /**
     * Adds a sidebar scoreboard on the player
     *
     * @param player
     * Player targeted
     */
    public void addPlayerScoreboard(final Player player) {
        ScoreboardManager manager = Bukkit.getScoreboardManager();
        Scoreboard board = manager.getNewScoreboard();
        Objective obj = board.registerNewObjective(DUY.TAG, "dummy", DUY.TAG);
        obj.setDisplaySlot(DisplaySlot.SIDEBAR);
        obj.getScore("Mined blocks :").setScore(1);

        inGamePlayers.get(player).getMinedBlocks().forEach((k, v) -> obj.getScore(v + "x + " + k).setScore(0));

        player.setScoreboard(board);
    }

    public void updateScoreboard(final Player player) {
        removePlayerScoreboard(player);
        addPlayerScoreboard(player);
    }

    /**
     * Removes the scoreboard from the player
     *
     * @param player
     * Player targeted
     */
    public void removePlayerScoreboard(final Player player) {
        player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
    }

    public void broadcastActionBarMessage(final BaseComponent[] text) {
        for (Player p : inGamePlayers.keySet())
            p.spigot().sendMessage(ChatMessageType.ACTION_BAR, text);
    }

    public void broadcastSound(final SoundData sound) {
        for (Player p : inGamePlayers.keySet())
            sound.play(p);
    }

    public enum GameResponse {
        IN_GAME,
        NOT_IN_GAME,
        NO_LOCATION_AVAILABLE,
        SUCCESS
    }
}
