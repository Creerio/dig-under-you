/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.modules;

import fr.creerio.duy.DUY;
import fr.creerio.duy.utils.SoundData;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;

public class Config {

    /**
     * Plugin's instance, used to save and load
     */
    private static JavaPlugin plugin = null;

    /**
     * Plugin's Config file instance, used to read specific values
     */
    private static FileConfiguration file = null;

    private static int taskId = -1;

    /**
     * Only constructor, must be used else we're not able to do anything with the Config
     *
     * @param mainPlugin
     * Config main instance
     */
    public Config(JavaPlugin mainPlugin) {
        plugin = mainPlugin;
        file = plugin.getConfig();
        loadDefaultConfig();
        file.options().copyDefaults(true);
        plugin.saveConfig();

        // Prepare language file
        Language.createInstance(plugin);
    }

    public static FileConfiguration getFile() {
        return file;
    }

    public static void load(){
        plugin.reloadConfig();
        handleSaveType();
    }

    public static void save(){
        file.set(ConfigKey.SPAWN_LOCATIONS_TAG.toString(), SpawnLocation.getInstance().getSerialisableLocations());
        file.set(ConfigKey.STARTING_ITEMS_TAG.toString(), GameUtils.getInstance().getStartingItems());
        file.set(ConfigKey.BOTTOM_BLOCKS_TAG.toString(), GameUtils.getInstance().getSerializableBottomBlocks());
        file.set(ConfigKey.TOP_BLOCKS_TAG.toString(), GameUtils.getInstance().getSerializableTopBlocks());
        plugin.saveConfig();
        Bukkit.getLogger().info(() -> DUY.TAG + " " + Language.getTranslation(Language.TranslationKey.SAVE_COMPLETED));
    }

    public static void handleSaveType() {
        if (taskId != -1) {
            Bukkit.getServer().getScheduler().cancelTask(taskId);
            taskId = -1;
            Bukkit.getLogger().info(() -> DUY.TAG + " " + Language.getTranslation(Language.TranslationKey.SAVE_TASK_END));
        }

        if (SaveType.getType(Config.getFile().getString(ConfigKey.SAVE_TYPE.toString())) == SaveType.PERIODIC) {
            taskId = Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(plugin, Config::save, 0, 20L * file.getInt(ConfigKey.SAVE_WHEN.toString()));
            Bukkit.getLogger().info(() -> DUY.TAG + " " + Language.getTranslation(Language.TranslationKey.SAVE_TASK_START));
        }
    }

    private void loadDefaultConfig() {
        file.addDefault(ConfigKey.MAX_HEIGHT_TAG.toString(), 10);
        file.addDefault(ConfigKey.LAVA_START_TAG.toString(), 5);
        file.addDefault(ConfigKey.MAINTENANCE_TAG.toString(), true);
        file.addDefault(ConfigKey.ENABLE_GAME_MESSAGES.toString(), true);
        file.addDefault(ConfigKey.SPAWN_LOCATIONS_TAG.toString(), new ArrayList<>());
        file.addDefault(ConfigKey.STARTING_ITEMS_TAG.toString(), new ArrayList<>());
        file.addDefault(ConfigKey.TOP_BLOCKS_TAG.toString(), new ArrayList<>());
        file.addDefault(ConfigKey.BOTTOM_BLOCKS_TAG.toString(), new ArrayList<>());

        file.addDefault(ConfigKey.SOUND_ENABLED.toString(), true);
        file.addDefault(ConfigKey.SOUND_ERROR.toString(), new SoundData(Sound.ENTITY_ENDERMAN_TELEPORT, 0.75f, 0));
        file.addDefault(ConfigKey.SOUND_SUCCESS.toString(), new SoundData(Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 0.75f, 0));
        file.addDefault(ConfigKey.SOUND_LAVA_SPAWN.toString(), new SoundData(Sound.ENTITY_WITHER_SPAWN, 0.75f, 0));
        file.addDefault(ConfigKey.SOUND_PLAYER_JOIN.toString(), new SoundData(Sound.ENTITY_ITEM_PICKUP, 0.75f, 0));

        file.addDefault(ConfigKey.RESPAWN_ENABLED.toString(), false);
        file.addDefault(ConfigKey.RESPAWN_LOCATION.toString(), null);

        file.addDefault(ConfigKey.SAVE_TYPE.toString(), SaveType.PERIODIC.toString());
        file.addDefault(ConfigKey.SAVE_WHEN.toString(), 600);
    }

    public enum ConfigKey {
        MAX_HEIGHT_TAG("maxHeight"),
        LAVA_START_TAG("lavaStart"),
        MAINTENANCE_TAG("maintenance"),
        SPAWN_LOCATIONS_TAG("spawnLocations"),
        STARTING_ITEMS_TAG("startingItems"),
        TOP_BLOCKS_TAG("topBlocks"),
        BOTTOM_BLOCKS_TAG("bottomBlocks"),
        ENABLE_GAME_MESSAGES("enableGameMessages"),
        SOUND_ENABLED("sound.enable"),
        SOUND_ERROR("sound.error"),
        SOUND_SUCCESS("sound.success"),
        SOUND_LAVA_SPAWN("sound.lavaspawn"),
        SOUND_PLAYER_JOIN("sound.playerJoin"),
        RESPAWN_ENABLED("respawn.enable"),
        RESPAWN_LOCATION("respawn.location"),
        SAVE_TYPE("save.type"),
        SAVE_WHEN("save.when");

        private final String key;

        ConfigKey(String confKey) {
            key = confKey;
        }

        @Override
        public String toString() {
            return key;
        }
    }
}
