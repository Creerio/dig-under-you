/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.modules;

import fr.creerio.duy.commands.Game;
import fr.creerio.duy.commands.Settings;
import fr.creerio.duy.utils.commands.CommandBase;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.TabExecutor;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class CommandHandler {

    private final List<CommandBase> commands = new ArrayList<>();

    /**
     * Constructor for single instances of a command
     * Add commands here
     */
    public CommandHandler() {
        commands.add(new Settings());
        commands.add(new Game());
    }

    public void addCommandsToPlugin(JavaPlugin mainPlugin) {
        for (CommandBase cmd : commands) {
            PluginCommand plCmd = mainPlugin.getCommand(cmd.getName());

            // To add commands, they must be inside the plugin.yml file
            assert plCmd != null;

            if (cmd instanceof CommandExecutor)
                plCmd.setExecutor((CommandExecutor) cmd);

            if (cmd instanceof TabExecutor)
                plCmd.setTabCompleter((TabExecutor) cmd);
        }
    }

}
