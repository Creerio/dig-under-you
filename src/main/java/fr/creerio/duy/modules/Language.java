/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.modules;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.EnumMap;
import java.util.Map;

public class Language {

    /**
     * Plugin's instance
     */
    private static JavaPlugin plugin = null;

    private static final String URL = "language.yml";

    private static final Map<TranslationKey, String> TRANSLATIONS = new EnumMap<>(TranslationKey.class);

    private static FileConfiguration config = null;
    private static File configFile = null;

    private Language(final JavaPlugin plg) {
        plugin = plg;

        reloadConfig();
    }

    public static void createInstance(final JavaPlugin plg) {
        if (plugin == null)
            new Language(plg);
    }

    public static void reloadConfig() {
        if (configFile == null)
            configFile = new File(plugin.getDataFolder(), URL);

        config = YamlConfiguration.loadConfiguration(configFile);

        // Look for defaults in the jar
        InputStream fileStream = plugin.getResource(URL);
        if (fileStream != null) {
            Reader defConfigStream = new InputStreamReader(fileStream, StandardCharsets.UTF_8);
            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
            config.setDefaults(defConfig);
        }

        // Save keys & values for easy access
        TRANSLATIONS.clear();
        for (TranslationKey key : TranslationKey.values()) {
            String val = config.getString(key.toString());
            if (val == null)
                config.set(key.toString(), "");
            TRANSLATIONS.put(key, val);
        }

        // Load pre-made file
        if (!configFile.exists()) {
            try {
                plugin.saveResource(URL, true);
            }
            catch (IllegalArgumentException e) {
                Bukkit.getLogger().severe(e.toString());
                save();
            }
        }

        // Keeps old keys & values, adds missing keys (with no data)
        if (TRANSLATIONS.containsValue(null))
            save();
    }

    private static void save() {
        try {
            config.save(configFile);
        }
        catch (IOException ex) {
            Bukkit.getLogger().severe("Failed to save new language file");
            Bukkit.getLogger().severe(ex.toString());
        }
    }

    public static String getTranslation(final TranslationKey key) {
        return TRANSLATIONS.get(key);
    }
    public static String getTranslation(final TranslationKey key, String ... args) {
        return String.format(TRANSLATIONS.get(key), (Object[]) args);
    }

    public enum TranslationKey {
        BLOCKS_DESCRIPTION("cmd.blocks.description"),
        BLOCKS_ADD_DESCRIPTION("cmd.blocks.add.description"),
        BLOCKS_ADD_UNKNOWN_MATERIAL("cmd.blocks.add.unknownMaterial"),
        BLOCKS_ADD_MISSING_HEIGHT("cmd.blocks.add.missingHeight"),
        BLOCKS_ADD_TOP_ALREADY_STORED("cmd.blocks.add.topAlreadyStored"),
        BLOCKS_ADD_BOTTOM_ALREADY_STORED("cmd.blocks.add.bottomAlreadyStored"),
        BLOCKS_ADD_TOP_ADDED("cmd.blocks.add.topAdded"),
        BLOCKS_ADD_BOTTOM_ADDED("cmd.blocks.add.bottomAdded"),
        BLOCKS_LIST_DESCRIPTION("cmd.blocks.list.description"),
        BLOCKS_LIST_TOP_BLOCKS("cmd.blocks.list.topBlocks"),
        BLOCKS_LIST_BOTTOM_BLOCKS("cmd.blocks.list.bottomBlocks"),
        BLOCKS_REMOVE_DESCRIPTION("cmd.blocks.remove.description"),
        BLOCKS_REMOVE_TOP_BLOCKS_NOT_STORED("cmd.blocks.remove.topBlockNotStored"),
        BLOCKS_REMOVE_BOTTOM_BLOCKS_NOT_STORED("cmd.blocks.remove.bottomBlockNotStored"),
        BLOCKS_REMOVE_SUCCESS("cmd.blocks.remove.success"),
        BLOCKS_TEST_DESCRIPTION("cmd.blocks.test.description"),
        BLOCKS_NO_TOP_BLOCK("cmd.blocks.noTopBlock"),
        BLOCKS_NO_BOTTOM_BLOCK("cmd.blocks.noBottomBlock"),
        BLOCKS_GENERATED("cmd.blocks.generated"),
        JOIN_DESCRIPTION("cmd.join.description"),
        JOIN_NO_LOCATION("cmd.join.noLocation"),
        JOIN_ALREADY_IN_GAME("cmd.join.alreadyInGame"),
        LEAVE_DESCRIPTION("cmd.leave.description"),
        LEAVE_NOT_IN_GAME("cmd.leave.notInGame"),
        LIST_NO_PLAYER("cmd.list.noPlayer"),
        LIST_PLAYERS("cmd.list.players"),
        LIST_DESCRIPTION("cmd.list.description"),
        HEIGHT_DESCRIPTION("cmd.height.description"),
        HEIGHT_GET_MAX_HEIGHT("cmd.height.get.maxHeight"),
        HEIGHT_GET_LAVA_START("cmd.height.get.lavaStart"),
        HEIGHT_INCOMPATIBLE_LOCATIONS("cmd.height.incompatible.locations"),
        HEIGHT_INCOMPATIBLE_MAX_HEIGHT("cmd.height.incompatible.maxHeight"),
        HEIGHT_INCOMPATIBLE_LAVA_START("cmd.height.incompatible.lavaStart"),
        HEIGHT_SAVED_MAX_HEIGHT("cmd.height.saved.maxHeight"),
        HEIGHT_SAVED_LAVA_START("cmd.height.saved.lavaStart"),
        ITEMS_DESCRIPTION("cmd.items.description"),
        ITEMS_ADD_DESCRIPTION("cmd.items.add.description"),
        ITEMS_ADD_LIMIT("cmd.items.add.limit"),
        ITEMS_ADD_UNKNOWN_ITEM("cmd.items.add.unknownItem"),
        ITEMS_ADD_NO_ITEM_IN_HAND("cmd.items.add.noItemInHand"),
        ITEMS_ADD_ALREADY_STORED("cmd.items.add.alreadyStored"),
        ITEMS_ADD_SUCCESS("cmd.items.add.success"),
        ITEMS_GUI_DESCRIPTION("cmd.items.gui.description"),
        ITEMS_GUI_EDITING("cmd.items.gui.editing"),
        ITEMS_GUI_MAINTENANCE("cmd.items.gui.maintenance"),
        ITEMS_GUI_SAVED("cmd.items.gui.saved"),
        ITEMS_LIST_DESCRIPTION("cmd.items.list.description"),
        ITEMS_LIST_ITEMS("cmd.items.list.items"),
        ITEMS_REMOVE_DESCRIPTION("cmd.items.remove.description"),
        ITEMS_REMOVE_NOT_FOUND("cmd.items.remove.notFound"),
        ITEMS_REMOVE_SUCCESS("cmd.items.remove.success"),
        ITEMS_NO_ITEM("cmd.items.noItem"),
        LOCATION_DESCRIPTION("cmd.location.description"),
        LOCATION_ADD_DESCRIPTION("cmd.location.add.description"),
        LOCATION_ADD_INVALID_HEIGHT("cmd.location.add.invalidHeight"),
        LOCATION_ADD_ALREADY_STORED("cmd.location.add.alreadyStored"),
        LOCATION_ADD_SUCCESS("cmd.location.add.success"),
        LOCATION_LIST_DESCRIPTION("cmd.location.list.description"),
        LOCATION_LIST_LOCATIONS("cmd.location.list.locations"),
        LOCATION_REMOVE_DESCRIPTION("cmd.location.remove.description"),
        LOCATION_REMOVE_SUCCESS("cmd.location.remove.success"),
        LOCATION_SHOW_DESCRIPTION("cmd.location.show.description"),
        LOCATION_SHOW_ENABLED("cmd.location.show.enabled"),
        LOCATION_SHOW_DISABLED("cmd.location.show.disabled"),
        LOCATION_TP_DESCRIPTION("cmd.location.tp.description"),
        LOCATION_TP_SUCCESS("cmd.location.tp.success"),
        LOCATION_NO_LOCATION("cmd.location.noLocation"),
        LOCATION_NOT_FOUND("cmd.location.notFound"),
        MAINTENANCE_DESCRIPTION("cmd.maintenance.description"),
        MAINTENANCE_CURRENT("cmd.maintenance.current"),
        MAINTENANCE_SAVED("cmd.maintenance.saved"),
        RELOAD_DESCRIPTION("cmd.reload.description"),
        RELOAD_SUCCESS_LANGUAGE("cmd.reload.successLanguage"),
        RELOAD_SUCCESS_CONFIG("cmd.reload.successConfig"),
        RESPAWN_DESCRIPTION("cmd.respawn.description"),
        RESPAWN_ENABLE_DESCRIPTION("cmd.respawn.enable.description"),
        RESPAWN_ENABLE("cmd.respawn.enabled"),
        RESPAWN_DISABLE_DESCRIPTION("cmd.respawn.disable.description"),
        RESPAWN_DISABLE("cmd.respawn.disabled"),
        RESPAWN_GET_DESCRIPTION("cmd.respawn.get.description"),
        RESPAWN_MODIFY_DESCRIPTION("cmd.respawn.modify.description"),
        RESPAWN_TEST_DESCRIPTION("cmd.respawn.test.description"),
        RESPAWN_STATE("cmd.respawn.state"),
        RESPAWN_NO_LOCATION("cmd.respawn.noLocation"),
        RESPAWN_LOCATION("cmd.respawn.location"),
        RESPAWN_MODIFY_SUCCESS("cmd.respawn.modify.success"),
        SAVE_DESCRIPTION("cmd.save.description"),
        SAVE_SUCCESS("cmd.save.success"),
        SAVE_PERIODIC_TIME("cmd.save.periodicTime"),
        SOUND_DESCRIPTION("cmd.sound.description"),
        SOUND_ENABLE_DESCRIPTION("cmd.sound.enable.description"),
        SOUND_ENABLE("cmd.sound.enabled"),
        SOUND_DISABLE_DESCRIPTION("cmd.sound.disable.description"),
        SOUND_DISABLE("cmd.sound.disabled"),
        SOUND_GET_DESCRIPTION("cmd.sound.get.description"),
        SOUND_MODIFY_DESCRIPTION("cmd.sound.modify.description"),
        SOUND_TEST_DESCRIPTION("cmd.sound.test.description"),
        SOUND_STATE("cmd.sound.state"),
        SOUND_LIST("cmd.sound.list"),
        SOUND_NOT_EXIST("cmd.sound.notExist"),
        SOUND_SAVED("cmd.sound.saved"),
        SOUND_PLAYED("cmd.sound.played"),
        IN_GAME_JOIN("inGame.join"),
        IN_GAME_SURVIVED_NOTHING("inGame.survived.nothing"),
        IN_GAME_SURVIVED_BLOCKS("inGame.survived.blocks"),
        IN_GAME_BLOCK_BREAK("inGame.block.breaking"),
        IN_GAME_BLOCK_PLACING("inGame.block.placing"),
        IN_GAME_GAMEMODE_CHANGED("inGame.gamemodeChanged"),
        IN_GAME_DIED("inGame.died"),
        INVALID_HEIGHT("invalid.height"),
        INVALID_MATERIAL("invalid.material"),
        INVALID_STARTUP_HEIGHT("invalid.startup.height"),
        INVALID_STARTUP_LOCATION("invalid.startup.location"),
        INVALID_STARTUP_TOP_BLOCK("invalid.startup.topBlock"),
        INVALID_STARTUP_BOTTOM_BLOCK("invalid.startup.bottomBlock"),
        INVALID_STARTUP_TOO_MUCH_ITEMS("invalid.startup.tooMuchItems"),
        MISSING_ARGUMENT("missing.argument"),
        MISSING_ARGUMENTS("missing.arguments"),
        MISSING_BLOCKS("missing.blocks"),
        MISSING_RESPAWN_LOCATION("missing.respawnLocation"),
        MISSING_PERMISSIONS("missing.permissions"),
        ZERO("number.zero"),
        ONE("number.one"),
        TWO("number.two"),
        THREE("number.three"),
        FOUR("number.four"),
        FIVE("number.five"),
        SIX("number.six"),
        DISABLED("other.disabled"),
        ENABLED("other.enabled"),
        ERROR("other.error"),
        MISSING_PLAYER("other.missingPlayer"),
        SUBCOMMAND_REQUIRES_PLAYER("other.subcommandRequiresPlayer"),
        LIST_SYMBOL("other.listSymbol"),
        LIST_SYMBOL_VARIANT("other.listSymbolVariant"),
        NOT_IDENTICAL_VALUE("other.notIdentical"),
        FAILED_READ_NUMBER("other.failedReadNumber"),
        LOCATION("other.location"),
        LOCATION_COMPLETE("other.locationComplete"),
        IN_GAME_PLAYER("other.inGamePlayer"),
        MAINTENANCE_BLOCK("other.maintenanceBlock"),
        MAINTENANCE_REQUIRED("other.maintenanceRequired"),
        POSITIVE_NUMBER_REQUIRED("other.positiveNumberRequired"),
        SAVE_TASK_START("save.task.start"),
        SAVE_TASK_END("save.task.end"),
        SAVE_COMPLETED("save.completed");

        private final String key;

        TranslationKey(String confKey) {
            key = confKey;
        }

        @Override
        public String toString() {
            return key;
        }
    }
}
