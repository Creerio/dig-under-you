/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands;

import fr.creerio.duy.commands.subcommands.game.Join;
import fr.creerio.duy.commands.subcommands.game.Leave;
import fr.creerio.duy.commands.subcommands.game.List;
import fr.creerio.duy.utils.commands.Command;
import fr.creerio.duy.utils.commands.SubCommandContainer;
import org.bukkit.command.CommandSender;

public class Game extends SubCommandContainer implements Command {

    public Game() {
        addSubCommands(new Join(), new Leave(), new List());
    }

    @Override
    public String getName() {
        return "game";
    }

    @Override
    public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label, String[] args) {
        // Subcommand only
        return onCommandHandleSub(sender, args);
    }

    @Override
    public java.util.List<String> onTabComplete(CommandSender sender, org.bukkit.command.Command command, String alias, String[] args) {
        return onTabCompleteHandleSub(sender, args);
    }
}
