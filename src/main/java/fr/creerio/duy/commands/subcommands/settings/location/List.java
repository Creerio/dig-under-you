/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings.location;

import fr.creerio.duy.modules.Language;
import fr.creerio.duy.modules.SpawnLocation;
import fr.creerio.duy.utils.commands.SimpleSubCommand;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;

public class List implements SimpleSubCommand {

    @Override
    public String getName() {
        return "list";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.LOCATION_LIST_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings spawn list";
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        java.util.List<Location> locs = SpawnLocation.getInstance().get();

        if (locs.isEmpty())
            sender.sendMessage(Language.getTranslation(Language.TranslationKey.LOCATION_NO_LOCATION));
        else {
            sender.sendMessage(Language.getTranslation(Language.TranslationKey.LOCATION_LIST_LOCATIONS));
            for (int i = 0; i < locs.size(); i++) {
                Location loc = locs.get(i);
                String locStr = Language.getTranslation(Language.TranslationKey.LOCATION, String.valueOf(loc.getX()), String.valueOf(loc.getY()), String.valueOf(loc.getZ()), (loc.getWorld() != null ? loc.getWorld().getName() : null));
                sender.sendMessage(Language.getTranslation(Language.TranslationKey.LIST_SYMBOL_VARIANT, String.valueOf(i), locStr));
            }
        }

        return true;
    }
}
