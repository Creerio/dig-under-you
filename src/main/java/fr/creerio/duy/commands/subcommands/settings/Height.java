/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings;

import fr.creerio.duy.modules.Config;
import fr.creerio.duy.modules.Config.ConfigKey;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.modules.SpawnLocation;
import fr.creerio.duy.utils.Utils;
import fr.creerio.duy.utils.commands.SubCommand;
import fr.creerio.duy.utils.commands.SubCommandContainer;
import org.bukkit.command.CommandSender;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Height extends SubCommandContainer implements SubCommand {

    @Override
    public String getName() {
        return "height";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.HEIGHT_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings height <maxheight/lavastart> [height]";
    }

    @Override
    public boolean isAllowed(CommandSender sender) {
        return sender.hasPermission("duy.settings.height");
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        // Wrong number of args
        if (args.length == 0 || args.length > 2)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.MISSING_ARGUMENTS, Language.getTranslation(Language.TranslationKey.ONE), Language.getTranslation(Language.TranslationKey.TWO)));

        // Show value requested
        if (args.length == 1) {
            if ("maxheight".equalsIgnoreCase(args[0]))
                sender.sendMessage(Language.getTranslation(Language.TranslationKey.HEIGHT_GET_MAX_HEIGHT, String.valueOf(Config.getFile().getInt(ConfigKey.MAX_HEIGHT_TAG.toString()))));
            else
                sender.sendMessage(Language.getTranslation(Language.TranslationKey.HEIGHT_GET_LAVA_START, String.valueOf(Config.getFile().getInt(ConfigKey.LAVA_START_TAG.toString()))));
        }
        // Modify it
        else {
            int val = Utils.getInt(sender, args[1]);

            if (val == Integer.MIN_VALUE)
                return false;

            // Locations must be compatible
            if (!SpawnLocation.getInstance().isHeightCompatible(val))
                return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.HEIGHT_INCOMPATIBLE_LOCATIONS));

            if ("maxHeight".equalsIgnoreCase(args[0])) {
                int lavaStart = Config.getFile().getInt(ConfigKey.LAVA_START_TAG.toString());

                // Lava start cannot be higher or equal to the max height
                if (lavaStart >= val)
                    return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.HEIGHT_INCOMPATIBLE_MAX_HEIGHT));

                Config.getFile().set(ConfigKey.MAX_HEIGHT_TAG.toString(), val);
                sender.sendMessage(Language.getTranslation(Language.TranslationKey.HEIGHT_SAVED_MAX_HEIGHT, String.valueOf(Config.getFile().getInt(ConfigKey.MAX_HEIGHT_TAG.toString()))));
            }
            else {
                int maxHeight = Config.getFile().getInt(ConfigKey.MAX_HEIGHT_TAG.toString());

                // Lava start cannot be higher or equal to the max height
                if (maxHeight <= val)
                    return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.HEIGHT_INCOMPATIBLE_LAVA_START));

                Config.getFile().set(ConfigKey.LAVA_START_TAG.toString(), val);
                sender.sendMessage(Language.getTranslation(Language.TranslationKey.HEIGHT_SAVED_LAVA_START, String.valueOf(Config.getFile().getInt(ConfigKey.LAVA_START_TAG.toString()))));
            }
        }

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, String[] args) {
        List<String> res;
        if (args.length <= 1) {
            res = Arrays.asList("maxheight", "lavastart");
            if (args.length == 1 && !args[0].isEmpty())
                res = res.stream().filter(s -> s.startsWith(args[0].toLowerCase())).collect(Collectors.toList());
        }
        else
            res = Collections.emptyList();

        return res;
    }
}
