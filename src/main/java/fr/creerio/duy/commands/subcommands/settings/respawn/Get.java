/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings.respawn;

import fr.creerio.duy.modules.Config;
import fr.creerio.duy.modules.Config.ConfigKey;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.utils.LocationSerializable;
import fr.creerio.duy.utils.commands.SimpleSubCommand;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;

public class Get implements SimpleSubCommand {

    @Override
    public String getName() {
        return "get";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.RESPAWN_GET_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings respawn get";
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        sender.sendMessage(Language.getTranslation(Language.TranslationKey.RESPAWN_STATE, (Config.getFile().getBoolean(ConfigKey.RESPAWN_ENABLED.toString()) ? Language.getTranslation(Language.TranslationKey.ENABLED) : Language.getTranslation(Language.TranslationKey.DISABLED))));

        LocationSerializable locSer = (LocationSerializable) Config.getFile().get(ConfigKey.RESPAWN_LOCATION.toString());

        if (locSer == null) {
            sender.sendMessage(Language.getTranslation(Language.TranslationKey.RESPAWN_NO_LOCATION));
        }
        else {
            Location loc = locSer.toLocation();
            String locStr = Language.getTranslation(Language.TranslationKey.LOCATION_COMPLETE, String.valueOf(loc.getX()), String.valueOf(loc.getY()), String.valueOf(loc.getZ()), (loc.getWorld() != null ? loc.getWorld().getName() : null), String.valueOf(loc.getYaw()), String.valueOf(loc.getPitch()));
            sender.sendMessage(Language.getTranslation(Language.TranslationKey.RESPAWN_LOCATION, locStr));
        }

        return true;
    }
}
