/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings.location;

import fr.creerio.duy.events.SpawnLocationAdd;
import fr.creerio.duy.modules.*;
import fr.creerio.duy.utils.Utils;
import fr.creerio.duy.utils.commands.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Add implements SubCommand {

    @Override
    public String getName() {
        return "add";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.LOCATION_ADD_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings spawn add [x] [y] [z] [world]";
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        Location toAdd;

        // A player can give less than 4 args to use current location, else wrong number of args
        if ( (!(sender instanceof Player) && args.length < 4) || args.length > 4)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.MISSING_ARGUMENT, Language.getTranslation(Language.TranslationKey.FOUR)));

        // Player location used
        if (sender instanceof Player && args.length < 4) {
            toAdd = ((Player) sender).getLocation();
            toAdd.setPitch(0f);
            toAdd.setYaw(0f);
        }
        else {
            // Coordinates may be relative
            double x = Double.parseDouble(args[0]);
            double y = Double.parseDouble(args[1]);
            double z = Double.parseDouble(args[2]);
            World world = Bukkit.getWorld(args[3]);
            toAdd = new Location(world, x, y, z);
        }

        // Height must be compatible
        if (!SpawnLocation.getInstance().validHeight(toAdd))
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.LOCATION_ADD_INVALID_HEIGHT));

        // Remove double digits & Center location
        toAdd.setX(toAdd.getBlockX() + 0.5);
        toAdd.setY(toAdd.getBlockY());
        toAdd.setZ(toAdd.getBlockZ() + 0.5);

        if (!SpawnLocation.getInstance().addLocation(toAdd))
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.LOCATION_ADD_ALREADY_STORED));

        String locStr = Language.getTranslation(Language.TranslationKey.LOCATION, String.valueOf(toAdd.getX()), String.valueOf(toAdd.getY()), String.valueOf(toAdd.getZ()), (toAdd.getWorld() != null ? toAdd.getWorld().getName() : null));
        sender.sendMessage(Language.getTranslation(Language.TranslationKey.LOCATION_ADD_SUCCESS, locStr));

        if (sender instanceof Player)
            Utils.playSuccessSound((Player) sender);

        // Event sent to generate armor stands if locations are shown
        Bukkit.getPluginManager().callEvent(new SpawnLocationAdd(toAdd));

        if (SaveType.getType(Config.getFile().getString(Config.ConfigKey.SAVE_TYPE.toString())) == SaveType.ON_UPDATE)
            Config.save();

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, String[] args) {
        if (!(sender instanceof Player))
            return Collections.emptyList();

        Location l = ((Player) sender).getLocation();
        List<String> res = new ArrayList<>();

        // Given position will depend on the player's current argument
        switch (args.length) {
            case 1:
                res.add(String.valueOf(l.getX()));
                if (!args[0].isEmpty())
                    res = res.stream().filter(s -> s.startsWith(args[0])).collect(Collectors.toList());
                break;
            case 2:
                res.add(String.valueOf(l.getY()));
                if (!args[1].isEmpty())
                    res = res.stream().filter(s -> s.startsWith(args[1])).collect(Collectors.toList());
                break;
            case 3:
                res.add(String.valueOf(l.getZ()));
                if (!args[2].isEmpty())
                    res = res.stream().filter(s -> s.startsWith(args[2])).collect(Collectors.toList());
                break;
            case 4:
                res.addAll(Bukkit.getWorlds().stream().map(World::getName).collect(Collectors.toList()));
                if (!args[3].isEmpty())
                    res = res.stream().filter(s -> s.startsWith(args[3])).collect(Collectors.toList());
                break;
            default:
                return Collections.emptyList();
        }

        return res;
    }
}
