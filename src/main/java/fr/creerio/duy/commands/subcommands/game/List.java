/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.game;

import fr.creerio.duy.modules.Game;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.utils.commands.SimpleSubCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class List implements SimpleSubCommand {

    @Override
    public String getName() {
        return "list";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.LIST_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "game list";
    }

    @Override
    public boolean isAllowed(CommandSender sender) {
        return sender.hasPermission("duy.game.list");
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        java.util.List<Player> inGame = Game.instance.playersInGame();

        if (inGame.isEmpty())
            sender.sendMessage(Language.getTranslation(Language.TranslationKey.LIST_NO_PLAYER));
        else {
            sender.sendMessage(Language.getTranslation(Language.TranslationKey.LIST_PLAYERS));
            inGame.forEach(player -> sender.sendMessage(Language.getTranslation(Language.TranslationKey.LIST_SYMBOL, player.getName())));
        }

        return true;
    }
}
