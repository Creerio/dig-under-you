/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings.sound;

import fr.creerio.duy.modules.Config;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.modules.SaveType;
import fr.creerio.duy.utils.SoundData;
import fr.creerio.duy.utils.Utils;
import fr.creerio.duy.utils.commands.SubCommand;
import fr.creerio.duy.utils.commands.SubCommandContainer;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.util.NumberConversions;

import java.util.*;
import java.util.stream.Collectors;

public class Modify extends SubCommandContainer implements SubCommand {

    @Override
    public String getName() {
        return "modify";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.SOUND_MODIFY_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings sound modify <sound type> <new sound> <volume> <pitch>";
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        // Wrong number of args
        if (args.length != 4)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.MISSING_ARGUMENT, Language.getTranslation(Language.TranslationKey.FOUR)));

        String sound = fr.creerio.duy.commands.subcommands.settings.Sound.getSoundConfigKey(args[0]);

        // Sound doesn't exist inside of MC
        if (sound == null)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.SOUND_NOT_EXIST));

        SoundData newSound = new SoundData(Sound.valueOf(args[1]), NumberConversions.toFloat(args[2]), NumberConversions.toFloat(args[3]));
        Config.getFile().set(sound, newSound);
        sender.sendMessage(Language.getTranslation(Language.TranslationKey.SOUND_SAVED, args[0], newSound.toString()));

        if (sender instanceof Player)
            Utils.playSuccessSound((Player) sender);

        if (SaveType.getType(Config.getFile().getString(Config.ConfigKey.SAVE_TYPE.toString())) == SaveType.ON_UPDATE)
            Config.save();

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, String[] args) {
        List<String> res;

        switch (args.length) {
            case 1:
                res = new ArrayList<>(fr.creerio.duy.commands.subcommands.settings.Sound.getSounds());
                if (!args[0].isEmpty())
                    res = res.stream().filter(s -> s.startsWith(args[0].toLowerCase())).collect(Collectors.toList());
                break;
            case 2:
                res = Arrays.stream(Sound.values()).map(Sound::toString).collect(Collectors.toList());
                if (!args[1].isEmpty())
                    res = res.stream().filter(s -> s.startsWith(args[1].toUpperCase())).collect(Collectors.toList());
                break;
            default:
                return Collections.emptyList();
        }

        return res;
    }
}
