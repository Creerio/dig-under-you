/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings.items;

import fr.creerio.duy.modules.Config;
import fr.creerio.duy.modules.GameUtils;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.modules.SaveType;
import fr.creerio.duy.utils.Utils;
import fr.creerio.duy.utils.commands.SubCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Remove implements SubCommand {

    @Override
    public String getName() {
        return "remove";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.ITEMS_REMOVE_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings items remove <item number>";
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        // Wrong number of args given
        if (args.length != 1)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.MISSING_ARGUMENT, Language.getTranslation(Language.TranslationKey.ONE)));

        int nb = Utils.getInt(sender, args[0]);

        // Arg given isn't a number
        if (nb == Integer.MIN_VALUE)
            return false;

        List<ItemStack> items = GameUtils.getInstance().getStartingItems();

        // No items, nothing to do
        if (items.isEmpty())
            sender.sendMessage(Language.getTranslation(Language.TranslationKey.ITEMS_NO_ITEM));
        else {
            // Item number must be in bounds to be removed
            ItemStack item;
            item = nb < 0 || items.size() <= nb ? null : items.get(nb);
            if (item == null || !GameUtils.getInstance().removeStartingItem(nb))
                return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.ITEMS_REMOVE_NOT_FOUND, String.valueOf(items.size() - 1)));

            sender.sendMessage(Language.getTranslation(Language.TranslationKey.ITEMS_REMOVE_SUCCESS, item.toString()));

            if (sender instanceof Player)
                Utils.playSuccessSound((Player) sender);

            if (SaveType.getType(Config.getFile().getString(Config.ConfigKey.SAVE_TYPE.toString())) == SaveType.ON_UPDATE)
                Config.save();
        }

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length != 1)
            return Collections.emptyList();

        List<ItemStack> items = GameUtils.getInstance().getStartingItems();
        if (items.isEmpty())
            return Collections.emptyList();

        List<String> itemNb = new ArrayList<>();
        for (int i = 0; i < items.size(); i++)
            itemNb.add(String.valueOf(i));

        if (!args[0].isEmpty())
            return itemNb.stream().filter(s -> s.startsWith(args[0])).collect(Collectors.toList());

        return itemNb;
    }
}
