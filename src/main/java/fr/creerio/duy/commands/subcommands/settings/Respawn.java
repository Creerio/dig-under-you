/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings;

import fr.creerio.duy.commands.subcommands.settings.respawn.*;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.utils.commands.SubCommand;
import fr.creerio.duy.utils.commands.SubCommandContainer;
import org.bukkit.command.CommandSender;

public class Respawn extends SubCommandContainer implements SubCommand {

    public Respawn() {
        addSubCommands(new Enable(), new Disable(), new Get(), new Modify(), new Test());
    }

    @Override
    public String getName() {
        return "respawn";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.RESPAWN_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings respawn <subcommand>";
    }

    @Override
    public boolean isAllowed(CommandSender sender) {
        return sender.hasPermission("duy.settings.respawn");
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        // Subcommands only
        return onCommandHandleSub(sender, args);
    }

    @Override
    public java.util.List<String> onTabComplete(CommandSender sender, String[] args) {
        return onTabCompleteHandleSub(sender, args);
    }
}
