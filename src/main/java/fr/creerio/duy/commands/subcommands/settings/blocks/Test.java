/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings.blocks;

import fr.creerio.duy.modules.Config;
import fr.creerio.duy.modules.Config.ConfigKey;
import fr.creerio.duy.modules.Game;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.modules.SpawnLocation;
import fr.creerio.duy.utils.Utils;
import fr.creerio.duy.utils.commands.SubCommand;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static fr.creerio.duy.commands.subcommands.settings.Blocks.BOTTOM_BLOCK;
import static fr.creerio.duy.commands.subcommands.settings.Blocks.TOP_BLOCK;

public class Test implements SubCommand {

    @Override
    public String getName() {
        return "test";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.BLOCKS_TEST_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings blocks test <" + TOP_BLOCK + "/" + BOTTOM_BLOCK + ">";
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        // Testing can only be done during a maintenance
        if (!Config.getFile().getBoolean(ConfigKey.MAINTENANCE_TAG.toString()))
            return Utils.sendMaintenanceRequirementMsg(sender);

        // Wrong number of args
        if (args.length != 1)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.MISSING_ARGUMENT, Language.getTranslation(Language.TranslationKey.ONE)));


        List<Location> locations = SpawnLocation.getInstance().get();

        // Generates blocks depending on the case
        if (TOP_BLOCK.equalsIgnoreCase(args[0])) {
            locations.forEach(Game.instance::generateTopBlocks);
        }
        else {
            // Method is deprecated to avoid people trying to use it (as it could allow players xraying to know when to leave), used here for testing purposes
            locations.forEach(Game.instance::generateBottomBlocks);
        }

        sender.sendMessage(Language.getTranslation(Language.TranslationKey.BLOCKS_GENERATED));
        Utils.playSuccessSound((Player) sender);

        return true;
    }

    @Override
    public java.util.List<String> onTabComplete(CommandSender sender, String[] args) {
        java.util.List<String> res = Arrays.asList(TOP_BLOCK, BOTTOM_BLOCK);

        if (args.length == 1 && !args[0].isEmpty())
            res = res.stream().filter(s -> s.startsWith(args[0].toLowerCase())).collect(Collectors.toList());

        return res;
    }
}
