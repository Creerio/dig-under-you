/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings;

import fr.creerio.duy.modules.Config;
import fr.creerio.duy.modules.GameUtils;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.modules.SpawnLocation;
import fr.creerio.duy.utils.Utils;
import fr.creerio.duy.utils.commands.SubCommand;
import fr.creerio.duy.utils.commands.SubCommandContainer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Reload extends SubCommandContainer implements SubCommand {

    @Override
    public String getName() {
        return "reload";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.RELOAD_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings reload <language/config>";
    }

    @Override
    public boolean isAllowed(CommandSender sender) {
        return sender.hasPermission("duy.settings.reload");
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        if (args.length != 1)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.MISSING_ARGUMENT, Language.getTranslation(Language.TranslationKey.ONE)));

        if (args[0].equals("language")) {
            Language.reloadConfig();
            sender.sendMessage(Language.getTranslation(Language.TranslationKey.RELOAD_SUCCESS_LANGUAGE));
        }
        else {
            // Config reload only allowed under maintenance to avoid breaking the game
            if (Config.getFile().getBoolean(Config.ConfigKey.MAINTENANCE_TAG.toString()))
                return Utils.sendMaintenanceRequirementMsg(sender);

            Config.load();
            SpawnLocation.reloadInstance();
            GameUtils.reloadInstance();
            sender.sendMessage(Language.getTranslation(Language.TranslationKey.RELOAD_SUCCESS_CONFIG));
        }

        if (sender instanceof Player)
            Utils.playSuccessSound((Player) sender);

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length != 1)
            return Collections.emptyList();

        List<String> res = Arrays.asList("language", "config");

        if (!args[0].isEmpty())
            res = res.stream().filter(s -> s.startsWith(args[0].toLowerCase())).collect(Collectors.toList());

        return res;
    }
}
