/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings.blocks;

import fr.creerio.duy.modules.GameUtils;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.utils.MaterialWHeight;
import fr.creerio.duy.utils.Utils;
import fr.creerio.duy.utils.commands.SubCommand;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;

import java.util.Arrays;
import java.util.stream.Collectors;

import static fr.creerio.duy.commands.subcommands.settings.Blocks.BOTTOM_BLOCK;
import static fr.creerio.duy.commands.subcommands.settings.Blocks.TOP_BLOCK;

public class List implements SubCommand {

    @Override
    public String getName() {
        return "list";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.BLOCKS_LIST_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings blocks list <" + TOP_BLOCK + "/" + BOTTOM_BLOCK + ">";
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        // Wrong number of args
        if (args.length != 1)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.MISSING_ARGUMENT, Language.getTranslation(Language.TranslationKey.ONE)));

        if (TOP_BLOCK.equalsIgnoreCase(args[0])) {
            java.util.List<Material> materials = GameUtils.getInstance().listTopBlocks();

            // No top blocks
            if (materials.isEmpty())
                sender.sendMessage(Language.getTranslation(Language.TranslationKey.BLOCKS_NO_TOP_BLOCK));
            else {
                sender.sendMessage(Language.getTranslation(Language.TranslationKey.BLOCKS_LIST_TOP_BLOCKS));
                for (Material mtr : materials) {
                    sender.sendMessage(Language.getTranslation(Language.TranslationKey.LIST_SYMBOL, mtr.toString()));
                }
            }
        }
        else {
            java.util.List<MaterialWHeight> materials = GameUtils.getInstance().listBottomBlocks();

            // No bottom blocks
            if (materials.isEmpty())
                sender.sendMessage(Language.getTranslation(Language.TranslationKey.BLOCKS_NO_BOTTOM_BLOCK));
            else {
                sender.sendMessage(Language.getTranslation(Language.TranslationKey.BLOCKS_LIST_BOTTOM_BLOCKS));
                for (int i = 0; i < materials.size(); i++)
                    sender.sendMessage(Language.getTranslation(Language.TranslationKey.LIST_SYMBOL_VARIANT, String.valueOf(i), materials.get(i).toString()));
            }
        }

        return true;
    }

    @Override
    public java.util.List<String> onTabComplete(CommandSender sender, String[] args) {
        java.util.List<String> res = Arrays.asList(TOP_BLOCK, BOTTOM_BLOCK);

        if (args.length == 1 && !args[0].isEmpty())
            res = res.stream().filter(s -> s.startsWith(args[0].toLowerCase())).collect(Collectors.toList());

        return res;
    }
}
