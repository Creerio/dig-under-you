/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings.sound;

import fr.creerio.duy.modules.Config;
import fr.creerio.duy.modules.Config.ConfigKey;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.modules.SaveType;
import fr.creerio.duy.utils.Utils;
import fr.creerio.duy.utils.commands.SimpleSubCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Disable implements SimpleSubCommand {

    @Override
    public String getName() {
        return "disable";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.SOUND_DISABLE_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings sound disable";
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        Config.getFile().set(ConfigKey.SOUND_ENABLED.toString(), false);
        sender.sendMessage(Language.getTranslation(Language.TranslationKey.SOUND_DISABLE));

        if (sender instanceof Player)
            Utils.playSuccessSound((Player) sender);

        if (SaveType.getType(Config.getFile().getString(Config.ConfigKey.SAVE_TYPE.toString())) == SaveType.ON_UPDATE)
            Config.save();

        return true;
    }
}
