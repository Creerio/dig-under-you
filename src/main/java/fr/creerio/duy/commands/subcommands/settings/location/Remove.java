/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings.location;

import fr.creerio.duy.events.SpawnLocationRemove;
import fr.creerio.duy.modules.*;
import fr.creerio.duy.utils.Utils;
import fr.creerio.duy.utils.commands.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Remove implements SubCommand {

    @Override
    public String getName() {
        return "remove";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.LOCATION_REMOVE_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings spawn remove <location number>";
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        // Can't remove locations when player in game
        if (!Game.instance.isEmpty())
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.IN_GAME_PLAYER));

        // Wrong number of args
        if (args.length != 1)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.MISSING_ARGUMENT, Language.getTranslation(Language.TranslationKey.ONE)));

        int nb = Utils.getInt(sender, args[0]);

        // Arg given isn't a number
        if (nb == Integer.MIN_VALUE)
            return false;

        List<Location> locs = SpawnLocation.getInstance().get();

        // No location
        if (locs.isEmpty())
            sender.sendMessage(Language.getTranslation(Language.TranslationKey.LOCATION_NO_LOCATION));
        else {
            // Try to remove location
            Location loc = locs.get(nb);
            if (!SpawnLocation.getInstance().removeLocation(loc))
                return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.LOCATION_NOT_FOUND, String.valueOf((locs.size() - 1))));

            String locStr = Language.getTranslation(Language.TranslationKey.LOCATION, String.valueOf(loc.getX()), String.valueOf(loc.getY()), String.valueOf(loc.getZ()), (loc.getWorld() != null ? loc.getWorld().getName() : null));
            sender.sendMessage(Language.getTranslation(Language.TranslationKey.LOCATION_REMOVE_SUCCESS, locStr));
            // Event sent to generate armor stands if locations are shown
            Bukkit.getPluginManager().callEvent(new SpawnLocationRemove(loc));

            if (sender instanceof Player)
                Utils.playSuccessSound((Player) sender);

            if (SaveType.getType(Config.getFile().getString(Config.ConfigKey.SAVE_TYPE.toString())) == SaveType.ON_UPDATE)
                Config.save();
        }

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length != 1)
            return Collections.emptyList();

        List<Location> locs = SpawnLocation.getInstance().get();
        if (locs.isEmpty())
            return Collections.emptyList();

        List<String> spawnNb = new ArrayList<>();
        for (int i = 0; i < locs.size(); i++)
            spawnNb.add(String.valueOf(i));

        if (!args[0].isEmpty())
            return spawnNb.stream().filter(s -> s.startsWith(args[0])).collect(Collectors.toList());

        return spawnNb;
    }
}
