/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings.sound;

import fr.creerio.duy.modules.Config;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.utils.SoundData;
import fr.creerio.duy.utils.Utils;
import fr.creerio.duy.utils.commands.SubCommand;
import fr.creerio.duy.utils.commands.SubCommandContainer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Test extends SubCommandContainer implements SubCommand {

    @Override
    public String getName() {
        return "test";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.SOUND_TEST_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings sound test <sound type>";
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        // Only a player can hear sounds
        if (!(sender instanceof Player))
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.SUBCOMMAND_REQUIRES_PLAYER));

        // Wrong number of args
        if (args.length != 1)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.MISSING_ARGUMENT, Language.getTranslation(Language.TranslationKey.ONE)));

        String soundStr = fr.creerio.duy.commands.subcommands.settings.Sound.getSoundConfigKey(args[0]);

        if (soundStr == null)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.SOUND_NOT_EXIST));

        SoundData sound = (SoundData) Config.getFile().get(soundStr);
        sound.play((Player) sender);
        sender.sendMessage(Language.getTranslation(Language.TranslationKey.SOUND_PLAYED, sound.toString()));

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length != 1)
            return Collections.emptyList();

        List<String> res = new ArrayList<>(fr.creerio.duy.commands.subcommands.settings.Sound.getSounds());
        if (!args[0].isEmpty())
            res = res.stream().filter(s -> s.startsWith(args[0].toLowerCase())).collect(Collectors.toList());

        return res;
    }
}
