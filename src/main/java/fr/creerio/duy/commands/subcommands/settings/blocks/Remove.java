/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings.blocks;

import fr.creerio.duy.modules.*;
import fr.creerio.duy.utils.Utils;
import fr.creerio.duy.utils.commands.SubCommand;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static fr.creerio.duy.commands.subcommands.settings.Blocks.BOTTOM_BLOCK;
import static fr.creerio.duy.commands.subcommands.settings.Blocks.TOP_BLOCK;

public class Remove implements SubCommand {

    @Override
    public String getName() {
        return "remove";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.BLOCKS_REMOVE_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings blocks remove <" + TOP_BLOCK + "/" + BOTTOM_BLOCK + "> <blockMaterial/number>";
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        // No player must be in the game when removing, as the blocks are generated when a player breaks a block
        if (!Game.instance.isEmpty())
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.IN_GAME_PLAYER));

        // Wrong number of args
        if (args.length != 2)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.MISSING_ARGUMENT, Language.getTranslation(Language.TranslationKey.TWO)));

        if (TOP_BLOCK.equalsIgnoreCase(args[0])) {
            Material mtr = Material.getMaterial(args[1].toUpperCase());

            // Not a valid material
            if (mtr == null || !GameUtils.isValidMaterial(mtr))
                return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.INVALID_MATERIAL));

            if (!GameUtils.getInstance().removeTopBlock(mtr))
                return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.BLOCKS_REMOVE_TOP_BLOCKS_NOT_STORED));
        }
        else {
            // Value given is invalid
            if (Utils.getInt(sender, args[1]) == Integer.MIN_VALUE)
                return false;

            if (!GameUtils.getInstance().removeBottomBlock(Utils.getInt(sender, args[1])))
                return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.BLOCKS_REMOVE_BOTTOM_BLOCKS_NOT_STORED));
        }

        sender.sendMessage(Language.getTranslation(Language.TranslationKey.BLOCKS_REMOVE_SUCCESS, args[0], args[1]));

        if (sender instanceof Player)
            Utils.playSuccessSound((Player) sender);

        if (SaveType.getType(Config.getFile().getString(Config.ConfigKey.SAVE_TYPE.toString())) == SaveType.ON_UPDATE)
            Config.save();

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length < 1 || args.length > 2)
            return Collections.emptyList();

        List<String> res;
        if (args.length <= 1) {
            res = Arrays.asList(TOP_BLOCK, BOTTOM_BLOCK);
            if (!args[0].isEmpty())
                res = res.stream().filter(s -> s.startsWith(args[0].toLowerCase())).collect(Collectors.toList());
        }
        else {
            if (TOP_BLOCK.equalsIgnoreCase(args[0])) {
                res = GameUtils.getInstance().listTopBlocks().stream().map(Material::toString).collect(Collectors.toList());
            }
            else {
                res = new ArrayList<>();
                for (int i = 0; i < GameUtils.getInstance().listBottomBlocks().size(); i++)
                    res.add(String.valueOf(i));
            }

            if (!args[1].isEmpty())
                res = res.stream().filter(s -> s.startsWith(args[1].toUpperCase())).collect(Collectors.toList());
        }


        return res;
    }
}
