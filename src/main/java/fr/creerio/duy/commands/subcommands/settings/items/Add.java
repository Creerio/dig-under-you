/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings.items;

import fr.creerio.duy.modules.Config;
import fr.creerio.duy.modules.GameUtils;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.modules.SaveType;
import fr.creerio.duy.utils.Utils;
import fr.creerio.duy.utils.commands.SubCommand;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Add implements SubCommand {

    @Override
    public String getName() {
        return "add";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.ITEMS_ADD_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings items add [item]";
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        // 8 starting items max
        if (GameUtils.getInstance().numberOfStartingItems() >= 8)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.ITEMS_ADD_LIMIT));

        ItemStack toAdd;

        // One arg means we read the basic material from what is given to us
        if (args.length == 1) {
            Material mtr = Material.getMaterial(args[0].toUpperCase());
            if (mtr == null)
                return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.ITEMS_ADD_UNKNOWN_ITEM));

            toAdd = new ItemStack(mtr);
        }
        // More means we want to fetch a more complex item from the player's main hand
        else {
            if (!(sender instanceof Player))
                return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.SUBCOMMAND_REQUIRES_PLAYER));

            toAdd = ((Player) sender).getInventory().getItemInMainHand();

            if (toAdd.getType() == Material.AIR)
                return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.ITEMS_ADD_NO_ITEM_IN_HAND));
        }

        if (!GameUtils.getInstance().addStartingItem(toAdd))
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.ITEMS_ADD_ALREADY_STORED));

        sender.sendMessage(Language.getTranslation(Language.TranslationKey.ITEMS_ADD_SUCCESS, toAdd.toString()));

        if (sender instanceof Player)
            Utils.playSuccessSound((Player) sender);

        if (SaveType.getType(Config.getFile().getString(Config.ConfigKey.SAVE_TYPE.toString())) == SaveType.ON_UPDATE)
            Config.save();

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length != 1)
            return Collections.emptyList();

        List<String> res = Arrays.stream(Material.values()).filter(mat -> !mat.isBlock()).map(Material::toString).collect(Collectors.toList());

        res.removeAll(GameUtils.getInstance().getStartingItems().stream().map(itemStack -> itemStack.getType().toString()).collect(Collectors.toList()));

        if (!args[0].isEmpty())
            res = res.stream().filter(s -> s.startsWith(args[0].toUpperCase())).collect(Collectors.toList());

        return res;
    }
}
