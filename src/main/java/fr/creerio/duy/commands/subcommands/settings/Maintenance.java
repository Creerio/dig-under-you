/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings;

import fr.creerio.duy.modules.*;
import fr.creerio.duy.modules.Config.ConfigKey;
import fr.creerio.duy.utils.Utils;
import fr.creerio.duy.utils.commands.SubCommand;
import fr.creerio.duy.utils.commands.SubCommandContainer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Maintenance extends SubCommandContainer implements SubCommand {

    @Override
    public String getName() {
        return "maintenance";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.MAINTENANCE_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings maintenance [on/off]";
    }

    @Override
    public boolean isAllowed(CommandSender sender) {
        return sender.hasPermission("duy.settings.maintenance");
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        if (args.length > 1)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.MISSING_ARGUMENTS, Language.getTranslation(Language.TranslationKey.ZERO), Language.getTranslation(Language.TranslationKey.ONE)));

        if (args.length == 0)
            sender.sendMessage(Language.getTranslation(Language.TranslationKey.MAINTENANCE_CURRENT) + (Config.getFile().getBoolean(ConfigKey.MAINTENANCE_TAG.toString()) ? Language.getTranslation(Language.TranslationKey.ENABLED) : Language.getTranslation(Language.TranslationKey.DISABLED)));
        else {
            String val = args[0].toLowerCase();
            if (!Pattern.matches("^(on|off)$", val))
                return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.NOT_IDENTICAL_VALUE));

            if (!"on".equals(val)) {
                SpawnLocation.getInstance().get().forEach(Game.instance::generateTopBlocks);
            }
            Config.getFile().set(ConfigKey.MAINTENANCE_TAG.toString(), "on".equals(val));
            sender.sendMessage(Language.getTranslation(Language.TranslationKey.MAINTENANCE_SAVED) + (Config.getFile().getBoolean(ConfigKey.MAINTENANCE_TAG.toString()) ? Language.getTranslation(Language.TranslationKey.ENABLED) : Language.getTranslation(Language.TranslationKey.DISABLED)));

            if (sender instanceof Player)
                Utils.playSuccessSound((Player) sender);

            if (SaveType.getType(Config.getFile().getString(Config.ConfigKey.SAVE_TYPE.toString())) == SaveType.ON_UPDATE)
                Config.save();
        }

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, String[] args) {
        List<String> res = Arrays.asList("on", "off");

        if (args.length == 1 && !args[0].isEmpty())
            res = res.stream().filter(s -> s.startsWith(args[0].toLowerCase())).collect(Collectors.toList());

        return res;
    }
}
