/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings.items;

import fr.creerio.duy.modules.Config;
import fr.creerio.duy.modules.Config.ConfigKey;
import fr.creerio.duy.events.inventories.Items;
import fr.creerio.duy.events.inventories.ItemsRead;
import fr.creerio.duy.modules.GameUtils;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.utils.Utils;
import fr.creerio.duy.utils.commands.SimpleSubCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Gui implements SimpleSubCommand {

    @Override
    public String getName() {
        return "gui";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.ITEMS_GUI_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings items gui";
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        // GUIs can only be used by players
        if (!(sender instanceof Player))
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.SUBCOMMAND_REQUIRES_PLAYER));

        java.util.List<ItemStack> items = GameUtils.getInstance().getStartingItems();

        // Someone already in the GUI/Server not under maintenance (when completed, this command CLEARS every starting item, we want to avoid players joining while this is happening)
        if (Items.isIsUsed() || !Config.getFile().getBoolean(ConfigKey.MAINTENANCE_TAG.toString())) {
            if (Items.isIsUsed())
                sender.sendMessage(Language.getTranslation(Language.TranslationKey.ITEMS_GUI_EDITING));
            else
                sender.sendMessage(Language.getTranslation(Language.TranslationKey.ITEMS_GUI_MAINTENANCE));
            ItemsRead.openInventory((Player) sender, items);
        }
        // Editable GUI opened
        else
            Items.openInventory((Player) sender, items);


        return true;
    }
}
