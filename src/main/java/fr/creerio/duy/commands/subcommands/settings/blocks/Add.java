/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings.blocks;

import fr.creerio.duy.modules.Config;
import fr.creerio.duy.modules.GameUtils;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.modules.SaveType;
import fr.creerio.duy.utils.MaterialWHeight;
import fr.creerio.duy.utils.Utils;
import fr.creerio.duy.utils.commands.SubCommand;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static fr.creerio.duy.commands.subcommands.settings.Blocks.BOTTOM_BLOCK;
import static fr.creerio.duy.commands.subcommands.settings.Blocks.TOP_BLOCK;
import static fr.creerio.duy.modules.GameUtils.ILLEGAL_BLOCK_TYPES;

public class Add implements SubCommand {

    @Override
    public String getName() {
        return "add";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.BLOCKS_ADD_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings blocks add <" + TOP_BLOCK + "/" + BOTTOM_BLOCK + "> [spawnHeight] [blockMaterial]";
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        // Not enough/Too many arguments given
        if (args.length < 1 || args.length > 3)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.MISSING_ARGUMENTS, Language.getTranslation(Language.TranslationKey.ONE), Language.getTranslation(Language.TranslationKey.THREE)));

        boolean isTopBlock = TOP_BLOCK.equalsIgnoreCase(args[0]);

        Material mtr;
        // Top blocks requires 2 args, bottom 3
        if ((isTopBlock && args.length == 2) || args.length == 3) {
            mtr = Material.getMaterial(isTopBlock ? args[1].toUpperCase() : args[2].toUpperCase());
            if (mtr == null)
                return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.BLOCKS_ADD_UNKNOWN_MATERIAL));
        }
        else {
            // No args, try to fetch material from main hand
            if (!(sender instanceof Player))
                return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.SUBCOMMAND_REQUIRES_PLAYER));

            mtr = ((Player) sender).getInventory().getItemInMainHand().getType();
        }

        // Bottom block material present, height missing
        if (!isTopBlock && args.length < 2)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.BLOCKS_ADD_MISSING_HEIGHT));

        // Not a block
        if (!GameUtils.isValidMaterial(mtr))
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.INVALID_MATERIAL));

        if (isTopBlock) {
            if (!GameUtils.getInstance().addTopBlock(mtr))
                return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.BLOCKS_ADD_TOP_ALREADY_STORED));

            sender.sendMessage(Language.getTranslation(Language.TranslationKey.BLOCKS_ADD_TOP_ADDED, mtr.toString()));
        }
        else {
            // Height is not an int
            if (Utils.getInt(sender, args[1]) == Integer.MIN_VALUE)
                return false;

            if (!GameUtils.getInstance().addBottomBlock(new MaterialWHeight(mtr, Utils.getInt(sender, args[1]))))
                return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.BLOCKS_ADD_BOTTOM_ALREADY_STORED));

            sender.sendMessage(Language.getTranslation(Language.TranslationKey.BLOCKS_ADD_BOTTOM_ADDED, mtr.toString(), args[1]));
        }

        if (sender instanceof Player)
            Utils.playSuccessSound((Player) sender);

        if (SaveType.getType(Config.getFile().getString(Config.ConfigKey.SAVE_TYPE.toString())) == SaveType.ON_UPDATE)
            Config.save();

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length < 1 || args.length > 3)
            return Collections.emptyList();

        List<String> res;

        if (args.length == 1) {
            res = Arrays.asList(TOP_BLOCK, BOTTOM_BLOCK);
            if (!args[0].isEmpty())
                res = res.stream().filter(s -> s.startsWith(args[0].toLowerCase())).collect(Collectors.toList());
        }
        else {
            boolean isTopBlock = TOP_BLOCK.equalsIgnoreCase(args[0]);
            if ((!isTopBlock && args.length == 2))
                return Collections.emptyList();

            res = Arrays.stream(Material.values()).filter(Material::isBlock).map(Material::toString).filter(s -> !Pattern.matches(ILLEGAL_BLOCK_TYPES, s)).collect(Collectors.toList());

            if (isTopBlock)
                res.removeAll(GameUtils.getInstance().listTopBlocks().stream().map(Material::toString).collect(Collectors.toList()));

            if (args.length == 2 && !args[1].isEmpty())
                res = res.stream().filter(s -> s.startsWith(args[1].toUpperCase())).collect(Collectors.toList());
        }

        return res;
    }
}
