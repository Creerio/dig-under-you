/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings.location;

import fr.creerio.duy.DUY;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.modules.SpawnLocation;
import fr.creerio.duy.utils.commands.SimpleSubCommand;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;

import java.util.ArrayList;
import java.util.List;

public class Show implements SimpleSubCommand {

    private static final List<ArmorStand> armorStands = new ArrayList<>();

    private static void spawnArmorStands(List<Location> locs) {
        if (locs == null) {
            locs = SpawnLocation.getInstance().get();
        }

        for (int i = 0; i < locs.size(); i++) {
            Location location = locs.get(i).clone();
            ArmorStand newStand = (ArmorStand) location.getWorld().spawnEntity(location, EntityType.ARMOR_STAND);
            newStand.setCustomName(String.valueOf(i));
            newStand.setCustomNameVisible(true);
            newStand.addScoreboardTag(DUY.TAG);
            newStand.setInvulnerable(true);
            newStand.setGlowing(true);
            newStand.setGravity(false);
            armorStands.add(newStand);
        }
    }

    public static void removeArmorStands() {
        for (ArmorStand stand : armorStands) {
            stand.remove();
        }
        armorStands.clear();
    }

    /**
     * Refresh the armor stands, if they are shown
     */
    public static void refresh() {
        if (!armorStands.isEmpty()) {
            removeArmorStands();
            spawnArmorStands(null);
        }
    }

    @Override
    public String getName() {
        return "show";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.LOCATION_SHOW_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings spawn show";
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        List<Location> locs = SpawnLocation.getInstance().get();

        if (locs.isEmpty())
            sender.sendMessage(Language.getTranslation(Language.TranslationKey.LOCATION_NO_LOCATION));
        else {
            // Show/Hide armor stands of stored locations
            if (armorStands.isEmpty()) {
                spawnArmorStands(locs);
                sender.sendMessage(Language.getTranslation(Language.TranslationKey.LOCATION_SHOW_ENABLED));
            }
            else {
                removeArmorStands();
                sender.sendMessage(Language.getTranslation(Language.TranslationKey.LOCATION_SHOW_DISABLED));
            }
        }

        return true;
    }
}
