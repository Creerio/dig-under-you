/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings.respawn;

import fr.creerio.duy.modules.Config;
import fr.creerio.duy.modules.Config.ConfigKey;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.modules.SaveType;
import fr.creerio.duy.utils.LocationSerializable;
import fr.creerio.duy.utils.Utils;
import fr.creerio.duy.utils.commands.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.*;
import java.util.stream.Collectors;

public class Modify implements SubCommand {

    @Override
    public String getName() {
        return "modify";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.RESPAWN_MODIFY_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings respawn modify [x] [y] [z] [world] [yaw] [pitch]";
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        // Respawn can be modified only under maintenance
        if (Config.getFile().getBoolean(ConfigKey.MAINTENANCE_TAG.toString()))
            return Utils.sendMaintenanceRequirementMsg(sender);

        Location toUse;

        // A player can use their current location, else wrong number of args
        if ( (!(sender instanceof Player) && args.length < 6) || args.length > 6)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.MISSING_ARGUMENT, Language.getTranslation(Language.TranslationKey.SIX)));

        // Use player's location w/ yaw & pitch
        if (sender instanceof Player && args.length == 0) {
            toUse = ((Player) sender).getLocation();
        }
        else {
            // Coordinates may be relative
            double x = Double.parseDouble(args[0]);
            double y = Double.parseDouble(args[1]);
            double z = Double.parseDouble(args[2]);
            World world = Bukkit.getWorld(args[3]);
            float yaw = Float.parseFloat(args[4]);
            float pitch = Float.parseFloat(args[5]);
            toUse = new Location(world, x, y, z, yaw, pitch);
        }

        // Location centered to block
        LocationSerializable loc = new LocationSerializable(toUse);
        loc.normalizePosition();
        toUse = loc.toLocation();

        Config.getFile().set(ConfigKey.RESPAWN_LOCATION.toString(), loc);
        String locStr = Language.getTranslation(Language.TranslationKey.LOCATION_COMPLETE, String.valueOf(toUse.getX()), String.valueOf(toUse.getY()), String.valueOf(toUse.getZ()), (toUse.getWorld() != null ? toUse.getWorld().getName() : null), String.valueOf(toUse.getYaw()), String.valueOf(toUse.getPitch()));
        sender.sendMessage(Language.getTranslation(Language.TranslationKey.RESPAWN_MODIFY_SUCCESS, locStr));

        if (sender instanceof Player)
            Utils.playSuccessSound((Player) sender);

        if (SaveType.getType(Config.getFile().getString(Config.ConfigKey.SAVE_TYPE.toString())) == SaveType.ON_UPDATE)
            Config.save();

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, String[] args) {
        if (!(sender instanceof Player))
            return Collections.emptyList();

        Location l = ((Player) sender).getLocation();
        List<String> res = new ArrayList<>();

        // Given position will depend on the player's current argument
        switch (args.length) {
            case 1:
                res.add(String.valueOf(l.getX()));
                if (!args[0].isEmpty())
                    res = res.stream().filter(s -> s.startsWith(args[0])).collect(Collectors.toList());
                break;
            case 2:
                res.add(String.valueOf(l.getY()));
                if (!args[1].isEmpty())
                    res = res.stream().filter(s -> s.startsWith(args[1])).collect(Collectors.toList());
                break;
            case 3:
                res.add(String.valueOf(l.getZ()));
                if (!args[2].isEmpty())
                    res = res.stream().filter(s -> s.startsWith(args[2])).collect(Collectors.toList());
                break;
            case 4:
                res.addAll(Bukkit.getWorlds().stream().map(World::getName).collect(Collectors.toList()));
                if (!args[3].isEmpty())
                    res = res.stream().filter(s -> s.startsWith(args[3])).collect(Collectors.toList());
                break;
            case 5:
                res.add(String.valueOf(((Player) sender).getLocation().getYaw()));
                if (!args[4].isEmpty())
                    res = res.stream().filter(s -> s.startsWith(args[4])).collect(Collectors.toList());
                break;
            case 6:
                res.add(String.valueOf(((Player) sender).getLocation().getPitch()));
                if (!args[5].isEmpty())
                    res = res.stream().filter(s -> s.startsWith(args[5])).collect(Collectors.toList());
                break;
            default:
                return Collections.emptyList();
        }

        return res;
    }
}
