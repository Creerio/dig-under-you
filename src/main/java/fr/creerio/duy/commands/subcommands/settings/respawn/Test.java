/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings.respawn;

import fr.creerio.duy.modules.Config;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.utils.LocationSerializable;
import fr.creerio.duy.utils.Utils;
import fr.creerio.duy.utils.commands.SimpleSubCommand;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Test implements SimpleSubCommand {

    @Override
    public String getName() {
        return "test";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.RESPAWN_TEST_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings respawn test";
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        // Only a player can test the respawn location as they will be teleported
        if (!(sender instanceof Player))
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.SUBCOMMAND_REQUIRES_PLAYER));

        LocationSerializable locSer = (LocationSerializable) Config.getFile().get(Config.ConfigKey.RESPAWN_LOCATION.toString());

        if (locSer == null)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.RESPAWN_NO_LOCATION));
        else {
            Location loc = locSer.toLocation();
            ((Player) sender).teleport(loc);
        }

        return true;
    }
}
