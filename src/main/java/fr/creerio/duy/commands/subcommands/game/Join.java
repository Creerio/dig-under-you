/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.game;

import fr.creerio.duy.modules.Config;
import fr.creerio.duy.modules.Config.ConfigKey;
import fr.creerio.duy.modules.GameUtils;
import fr.creerio.duy.modules.Game;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.utils.Utils;
import fr.creerio.duy.utils.commands.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Join implements SubCommand {

    @Override
    public String getName() {
        return "join";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.JOIN_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "game join [player]";
    }

    @Override
    public boolean isAllowed(CommandSender sender) {
        return sender.hasPermission("duy.game.join");
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        // Server under maintenance, no joining allowed
        if (Config.getFile().getBoolean(ConfigKey.MAINTENANCE_TAG.toString()))
            return Utils.sendMaintenanceMsg(sender);

        // Too many arguments given
        if (args.length > 1)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.MISSING_ARGUMENTS, Language.getTranslation(Language.TranslationKey.ZERO), Language.getTranslation(Language.TranslationKey.ONE)));

        // Invalid height stored, may cause issues
        if (Config.getFile().getInt(ConfigKey.MAX_HEIGHT_TAG.toString()) <= 0)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.INVALID_HEIGHT));

        // No minable blocks
        if (!GameUtils.getInstance().isUsable())
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.MISSING_BLOCKS));

        // No respawn location if the player finishes
        if (Config.getFile().getBoolean(ConfigKey.RESPAWN_ENABLED.toString()) && Config.getFile().get(ConfigKey.RESPAWN_LOCATION.toString()) == null)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.MISSING_RESPAWN_LOCATION));


        Player player;
        if (args.length == 0 || !sender.hasPermission("duy.game.forceJoin"))
            player = (Player) sender;
        else {
            player = Bukkit.getPlayer(args[0]);
            if (player == null)
                return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.MISSING_PLAYER, args[0]));
        }

        switch (Game.instance.addPlayer(player)) {
            case NO_LOCATION_AVAILABLE:
                return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.JOIN_NO_LOCATION));
            case IN_GAME:
                return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.JOIN_ALREADY_IN_GAME));
            default:
        }

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length == 1 && sender.hasPermission("duy.game.forceJoin")) {
            List<String> res = Bukkit.getOnlinePlayers().stream().map(HumanEntity::getName).collect(Collectors.toList());
            res.removeAll(Game.instance.playersInGame().stream().map(HumanEntity::getName).collect(Collectors.toList()));

            if (!args[0].isEmpty())
                return res.stream().filter(s -> s.startsWith(args[0])).collect(Collectors.toList());

            return res;
        }

        return Collections.emptyList();
    }
}
