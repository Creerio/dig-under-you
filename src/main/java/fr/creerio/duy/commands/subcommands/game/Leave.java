/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.game;

import fr.creerio.duy.modules.Game;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.utils.Utils;
import fr.creerio.duy.utils.commands.SubCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Leave implements SubCommand {

    @Override
    public String getName() {
        return "leave";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.LEAVE_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "game leave [player]";
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        // Too many arguments given
        if (args.length > 1)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.MISSING_ARGUMENTS, Language.getTranslation(Language.TranslationKey.ZERO), Language.getTranslation(Language.TranslationKey.ONE)));

        Player player;
        if (args.length == 0 || !sender.hasPermission("duy.game.forceLeave"))
            player = (Player) sender;
        else {
            player = Bukkit.getPlayer(args[0]);
            if (player == null)
                return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.MISSING_PLAYER, args[0]));
        }

        if (Game.instance.removePlayer(player, true) == Game.GameResponse.NOT_IN_GAME)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.LEAVE_NOT_IN_GAME));

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, String[] args) {
        if (Game.instance.isEmpty())
            return Collections.emptyList();

        if (args.length == 1 && sender.hasPermission("duy.game.forceLeave")) {
            List<String> res = Game.instance.playersInGame().stream().map(HumanEntity::getName).collect(Collectors.toList());

            if (!args[0].isEmpty())
                return res.stream().filter(s -> s.startsWith(args[0])).collect(Collectors.toList());

            return res;
        }

        return Collections.emptyList();
    }
}
