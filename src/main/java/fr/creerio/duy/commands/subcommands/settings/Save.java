/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings;

import fr.creerio.duy.modules.Config;
import fr.creerio.duy.modules.Config.ConfigKey;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.modules.SaveType;
import fr.creerio.duy.utils.Utils;
import fr.creerio.duy.utils.commands.SubCommand;
import fr.creerio.duy.utils.commands.SubCommandContainer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Save extends SubCommandContainer implements SubCommand {

    @Override
    public String getName() {
        return "save";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.SAVE_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings save <type/now> [when]";
    }

    @Override
    public boolean isAllowed(CommandSender sender) {
        return sender.hasPermission("duy.settings.save");
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        if (args.length < 1 || args.length > 2)
            return Utils.sendErrorMsg(sender, Language.getTranslation(Language.TranslationKey.MISSING_ARGUMENTS, Language.getTranslation(Language.TranslationKey.ONE), Language.getTranslation(Language.TranslationKey.TWO)));

        if (args[0].equals("now")) {
            Config.save();
            sender.sendMessage(Language.getTranslation(Language.TranslationKey.SAVE_COMPLETED));

        }
        else {
            SaveType saveType = SaveType.getType(args[0]);
            int when = -1;

            if (saveType == SaveType.PERIODIC) {
                if (args.length == 2 && !args[1].isEmpty()) {
                    when = Utils.getInt(sender, args[1]);
                    if (when == Integer.MIN_VALUE)
                        return false;
                }
                else
                    when = 300; // 10 min by default
            }

            Config.getFile().set(ConfigKey.SAVE_TYPE.toString(), saveType.toString());
            Config.getFile().set(ConfigKey.SAVE_WHEN.toString(), when);
            sender.sendMessage(Language.getTranslation(Language.TranslationKey.SAVE_SUCCESS, saveType.toString()) + (saveType == SaveType.PERIODIC ? Language.getTranslation(Language.TranslationKey.SAVE_PERIODIC_TIME, String.valueOf(when)) : ""));
            Config.handleSaveType();
        }

        if (sender instanceof Player)
            Utils.playSuccessSound((Player) sender);

        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, String[] args) {
        if (args.length != 1)
            return Collections.emptyList();

        List<String> res = Arrays.stream(SaveType.values()).map(SaveType::toString).collect(Collectors.toList());
        res.add("now");

        if (!args[0].isEmpty())
            res = res.stream().filter(s -> s.startsWith(args[0].toLowerCase())).collect(Collectors.toList());

        return res;
    }
}
