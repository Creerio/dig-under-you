/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings;

import fr.creerio.duy.modules.Config;
import fr.creerio.duy.commands.subcommands.settings.sound.*;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.utils.commands.SubCommand;
import fr.creerio.duy.utils.commands.SubCommandContainer;
import org.bukkit.command.CommandSender;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

public class Sound extends SubCommandContainer implements SubCommand {

    private static final Map<String, String> availableSounds = new HashMap<>();

    public Sound() {
        addSubCommands(new Enable(), new Disable(), new Get(), new Modify(), new Test());

        for (Config.ConfigKey key : Config.ConfigKey.values()) {
            String keyStr = key.toString();
            if (keyStr.startsWith("sound.") && !keyStr.endsWith("enable"))
                availableSounds.put(keyStr.replaceFirst("sound.", ""), keyStr);
        }
    }

    public static String getSoundConfigKey(String key) {
        return availableSounds.get(key);
    }

    public static Collection<String> getSoundConfigKeys() {
        return availableSounds.values();
    }

    public static Collection<String> getSounds() {
        return availableSounds.keySet();
    }

    /**
     * Executes an action for each sound
     * First string given is the sound name, the second for its config key
     *
     * @param consumer
     * 'Function' executed
     */
    public static void forEachSound(BiConsumer<String, String> consumer) {
        availableSounds.forEach(consumer);
    }

    @Override
    public String getName() {
        return "sound";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.SOUND_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings sound <subcommand>";
    }

    @Override
    public boolean isAllowed(CommandSender sender) {
        return sender.hasPermission("duy.settings.sound");
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        // Subcommands only
        return onCommandHandleSub(sender, args);
    }

    @Override
    public java.util.List<String> onTabComplete(CommandSender sender, String[] args) {
        return onTabCompleteHandleSub(sender, args);
    }


}
