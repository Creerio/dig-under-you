/*
 * DUY
 * Copyright (C) 2023 Creerio
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package fr.creerio.duy.commands.subcommands.settings.items;

import fr.creerio.duy.modules.GameUtils;
import fr.creerio.duy.modules.Language;
import fr.creerio.duy.utils.commands.SimpleSubCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.ItemStack;

public class List implements SimpleSubCommand {

    @Override
    public String getName() {
        return "list";
    }

    @Override
    public String getDescription() {
        return Language.getTranslation(Language.TranslationKey.ITEMS_LIST_DESCRIPTION);
    }

    @Override
    public String getSyntax() {
        return "settings items list";
    }

    @Override
    public boolean onSubCommand(CommandSender sender, String[] args) {
        java.util.List<ItemStack> items = GameUtils.getInstance().getStartingItems();

        if (items.isEmpty())
            sender.sendMessage(Language.getTranslation(Language.TranslationKey.ITEMS_NO_ITEM));
        else {
            sender.sendMessage(Language.getTranslation(Language.TranslationKey.ITEMS_LIST_ITEMS));
            for (int i = 0; i < items.size(); i++) {
                ItemStack item = items.get(i);
                sender.sendMessage(Language.getTranslation(Language.TranslationKey.LIST_SYMBOL_VARIANT, String.valueOf(i), item.toString()));
            }
        }

        return true;
    }
}
