cmd:
  join:
    description: Used to join the game
    noLocation: No location available for the player, please wait for other players
      to finish !
    alreadyInGame: This player is already in the game !
  leave:
    description: Used to leave the game
    notInGame: This player is not in the game !
  list:
    noPlayer: No player in the game
    players: 'Players in the game :'
    description: Gives a list of player in the game
  blocks:
    add:
      description: Adds blocks
      unknownMaterial: Unknown material provided !
      missingHeight: When providing a bottom block, a spawn height must be provided
        !
      topAlreadyStored: The material provided is already stored !
      topAdded: 'Added provided material : %1$s'
      bottomAlreadyStored: The provided material & height are already stored !
      bottomAdded: 'Added provided material : %1$s at height %2$s'
    list:
      description: List blocks used
      topBlocks: 'Top blocks :'
      bottomBlocks: 'Bottom blocks :'
    gui:
      description: Lists and/or allows modification of currently stored blocks
    remove:
      description: Removes blocks
      topBlockNotStored: The material provided isn't stored !
      bottomBlockNotStored: Provided position isn't associated with a block & height
        !
      success: 'Removed provided %1$s : %2$s'
    test:
      description: Tests required block type
    description: Handles blocks used in the game
    noTopBlock: No top block found. Add one with /settings blocks add topblock
    generated: Generated required blocks !
    noBottomBlock: No bottom block found. Add one with /settings blocks add bottomblock
  height:
    description: Allows modification of the height used to create the blocks under
      the player
    get:
      maxHeight: 'Blocks can go down to : %1$s'
      lavaStart: 'Lava can start to appear from : %1$s'
    incompatible:
      locations: Provided height value isn't compatible with currently stored locations.
        Please either remove some or use another value !
      maxHeight: Provided max height is lower or the same as the lava starting height.
        Please use a higher value !
      lavaStart: Provided lava starting height is higher or the same as the max height.
        Please use a lower value !
    saved:
      maxHeight: 'Saved new max height : %1$s'
      lavaStart: 'Saved new lava starting height : %1$s'
  items:
    add:
      description: Adds starting items, can take from main hand or given material
      limit: There can only be 8 starting items !
      unknownItem: Failed to retrieve given item !
      noItemInHand: No item in your main hand, please take one !
      alreadyStored: This item is already stored !
      success: 'Added given item : %1$s'
    gui:
      description: Lists and/or allows modification of currently stored starting items
      editing: Someone is editing the starting items... showing you a read-only list
      maintenance: For security reasons, this command cannot be used while the game
        isn't under maintenance. Showing you a read-only list
      saved: Saved starting items !
    list:
      description: Lists currently stored starting items
      items: 'Starting items :'
    remove:
      description: Removes a given starting item
      notFound: Starting item not found. Please provide an input between 0 and %1$s
      success: 'Removed given item : %1$s'
    description: Handles starting items used in the game
    noItem: No starting item found. Add one with /settings items add
  maintenance:
    description: Enables/Disables maintenance mode
    current: Maintenance is
    saved: Maintenance is now
  location:
    add:
      description: Adds spawn location
      invalidHeight: This location is invalid based on the height setting used ! Please
        put the starting location higher or modify the height value !
      alreadyStored: This location is already stored !
      success: '%1$s has been added to the spawn locations.'
    list:
      description: Lists currently stored spawn locations
      locations: 'Spawn locations :'
    remove:
      description: Removes a given location
      success: '%1$s has been removed from the spawn locations'
    show:
      description: Shows currently stored spawn locations
      enabled: Armor stands shown
      disabled: Armor stands hidden
    tp:
      description: Teleports you to a given location
      success: Teleported to %1$s
    description: Handles spawn locations
    noLocation: No spawn location found. Add one with /settings spawn add
    notFound: Location not found. Please provide an input between 0 and %1$s
  respawn:
    enable:
      description: Enables respawning after leaving/dying
    disable:
      description: Disables respawning after leaving/dying
    state: Respawning is %1$s
    noLocation: No location stored
    location: 'Respawn location : %1$s'
    modify:
      success: '%1$s is the new respawn location.'
      description: Modifies respawn location
    get:
      description: Gets current respawn location & if respawn is enabled
    test:
      description: Used to test teleport location
    description: Handles player respawn after leaving/dying
    enabled: Enabled respawning
    disabled: Disabled respawning
  save:
    description: Saves & modifies when to save the config
    success: Saved given settings, save type is %1$s
    periodicTime: and is done every %1$s seconds
  sound:
    enable:
      description: Enables game sounds
    disable:
      description: Disables game sounds
    state: Sounds are %1$s
    list: 'Sounds :'
    notExist: The given sound type doesn't exist
    saved: 'Saved new sound for %1$s : %2$s'
    played: Played sound %1$s
    get:
      description: Gets current game sounds & if sounds are enabled
    modify:
      description: Modifies game sounds
    test:
      description: Used to test game sounds
    description: Handles game sounds
    enabled: Enabled sounds
    disabled: Disabled sounds
  reload:
    description: Used to reload the config file or the translations
    successLanguage: Language reloaded
    successConfig: Config reloaded
inGame:
  join: '%1$s joined the game'
  survived:
    blocks: 'You survived with the following blocks :'
    nothing: You survived with nothing
  block:
    breaking: You can only break blocks under you !
    placing: You cannot place blocks !
  gamemodeChanged: Removed you from the game as you changed gamemode !
  died: You died in the lava, every item you had is now lost.
invalid:
  height: Current height is invalid ! Please tell someone with permissions to fix
    that.
  material: The material provided isn't a block, please use another one !
  startup:
    topBlock: 'Wrong material detected, removing provided : %s$1'
    bottomBlock: 'Wrong material detected, removing provided : %s$1'
    tooMuchItems: 'Found more items than allowed, removing them : %s$1'
    height: Wrong height detected in configuration. Switching height value to zero !
    location: 'Wrong location detected, removing from stored location => %s$1'
missing:
  argument: This command requires %1$s argument !
  arguments: This command requires %1$s to %2$s arguments !
  blocks: No block is defined for you to break ! Please tell someone with permissions
    to fix that.
  respawnLocation: Respawn is enabled but no location seems to be defined ! Please
    tell someone with permissions to fix that.
  permissions: You do not have the permission to do that !
number:
  zero: zero
  one: one
  two: two
  three: three
  four: four
  five: five
  six: six
other:
  disabled: disabled
  enabled: enabled
  error: 'Error : '
  missingPlayer: Failed to fetch %1$s, are they online ?
  listSymbol: '  - %1$s'
  listSymbolVariant: '  - %1$s => %2$s'
  notIdentical: Provided value doesn't correspond to allowed values !
  failedReadNumber: Failed to read your number.
  subcommandRequiresPlayer: Using this subcommand variation requires you to be a player
    !
  inGamePlayer: A player is in the game, please wait for them to finish first !
  location: '%1$s/%2$s/%3$s in world %4$s'
  locationComplete: '%1$s/%2$s/%3$s in world %4$s with yaw %5$s and pitch %6$s'
  maintenanceBlock: This game is under maintenance !
  maintenanceRequired: This command requires the game to be under maintenance !
  positiveNumberRequired: Provided height value cannot be a negative number or zero
    !
save:
  task:
    start: Started save task
    end: Stopped save task
  completed: Saved configuration
