# Dig Under You

A [Minecraft](https://www.minecraft.net/) Java Edition [Spigot](https://www.spigotmc.org/) plugin for a simple game.

## Game description

The player's goal is to dig as low as possible, without dying and with the possibility of bailing anytime. To do so, they are provided with some tools (or nothing) depending on the owner's choices. The mined blocks also depends on what 'materials' the owner chose.

## Warning

This plugin is more monolithic than it should be, meaning it can be used in any circumstances, with and without any other plugins. But, should you plan on including it on a mini-game server with a lot of players, I recommend you to remove some modules to keep only what is needed.

Also, this plugin has been tested with Paper 1.16.5 and *may* not work with newer Minecraft versions (or there may be no issue at all).

## Compiling

1. Clone this repo `git clone https://gitlab.com/Creerio/dig-under-you.git` and enter the created folder
2. Run `./gradlew build` on a Linux/Mac terminal or something like `gradlew.bat build` on Windows

## Download

No download provided, please compile the plugin using gradle and take the created jar file into your plugins folder.

## How to play?/How to use it?/Etc.

See the [wiki](https://gitlab.com/Creerio/dig-under-you/-/wikis/Home).

## Credits

- [Grian](https://www.youtube.com/@Grian) for creating the Dig Straight Down mini-game in [Hermitcraft](https://hermitcraft.fandom.com/) Season 6 (See [here](https://youtu.be/9W29XIHWZM4?t=52)), which this game is **heavily** inspired from. If you have some time, go watch some of his videos (Season 6 & 7 are a great pick) to support him.
